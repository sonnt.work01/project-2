<?php

use App\Http\Middleware\CheckRole;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Middleware\CheckLoginAdmin;
use App\Http\Middleware\CheckLoginCustomer;
use App\Http\Controllers\Admin_manager\AreaController;
use App\Http\Controllers\Admin_manager\BillController;
use App\Http\Controllers\Admin_manager\BlogController;
use App\Http\Controllers\Admin_manager\TimeController;
use App\Http\Controllers\Admin_manager\AdminController;
use App\Http\Controllers\Admin_manager\PitchController;
use App\Http\Controllers\Admin_manager\CustomerController;
use App\Http\Controllers\Customer_manager\OrderController;
use App\Http\Controllers\Admin_manager\DashboardController;
use App\Http\Controllers\Customer_manager\SignupController;
use App\Http\Controllers\Admin_manager\AdminLoginController;
use App\Http\Controllers\Admin_manager\AdminXAreaController;
use App\Http\Controllers\Admin_manager\OrderAdminController;
use App\Http\Controllers\Customer_manager\ContactController;
use App\Http\Controllers\Admin_manager\CustomBannerController;
use App\Http\Controllers\Admin_manager\ProfileAdminController;
use App\Http\Controllers\Customer_manager\IntroduceController;
use App\Http\Controllers\Customer_manager\BlogDetailController;
use App\Http\Controllers\Admin_manager\IntroduceAdminController;
use App\Http\Controllers\Customer_manager\BillCustomerController;
use App\Http\Controllers\Customer_manager\CustomerLoginController;
use App\Http\Controllers\Customer_manager\PitchCustomerController;
use App\Http\Controllers\Customer_manager\ChangePasswordController;
use App\Http\Controllers\Customer_manager\ProfileCustomerController;
use App\Http\Controllers\Admin_manager\ChangePasswordAdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//login admin
Route::get('/login_admin', [AdminLoginController::class, 'index'])->name('login-admin');
Route::post('/login_admin_process', [AdminLoginController::class, 'LoginProcess'])->name('login-admin-process');

//login customer
Route::get('/login', [CustomerLoginController::class, 'index'])->name('login');
Route::post('/login_process', [CustomerLoginController::class, 'LoginProcess'])->name('login-process');
Route::get('/log_out', [CustomerLoginController::class, 'LogOut'])->name('log-out');
//user

// //home
Route::get('/', [HomeController::class, 'index'])->name('home');
//web
Route::get('/pitch_page/{id}', [PitchCustomerController::class, 'index'])->name('pitch_page');
//introduce
Route::get('/about-us.html', [IntroduceController::class, 'index']);
Route::get('/contact-us', [ContactController::class, 'index']);

Route::get('/blog_detail/{id}', [BlogDetailController::class, 'index']);

// quen mat khau khach hang
Route::get('/forget_password.html', [CustomerLoginController::class, 'ForgetPass']);
Route::post('/process_forget_pass_customer', [CustomerLoginController::class, 'ProcessForgetPass']);

//quen mat khau admin
Route::get('/forget_password_admin.html', [AdminLoginController::class, 'ForgetPass']);
Route::post('/process_forget_pass_admin', [AdminLoginController::class, 'ProcessForgetPass']);


Route::middleware([CheckLoginCustomer::class])->group(function () {
    Route::get('/order/{area_id}/{pitch_id}/{search}', [OrderController::class, 'index'])->name('order');
    Route::post('/order_process', [OrderController::class, 'store'])->name('order-process');
    //
    Route::get('/bill_page', [BillCustomerController::class, 'index'])->name('bill-page');
    Route::post('/destroy_process/{bill_id}', [BillCustomerController::class, 'destroy_process']);

    Route::resource('profile_customer', ProfileCustomerController::class);
    Route::resource('change_password', ChangePasswordController::class);
});

Route::resource('signup', SignupController::class);
Route::middleware([CheckLoginAdmin::class])->group(function () {
    //admin
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::middleware([CheckRole::class])->group(function () {
        Route::resource('admin', AdminController::class);
        Route::resource('area', AreaController::class);
        Route::resource('introduce', IntroduceAdminController::class);
        Route::resource('admin_x_area', AdminXAreaController::class);
    });
    Route::get('/order_admin/{area_id}/{pitch_id}/{search}', [OrderAdminController::class, 'index']);
    Route::post('/order_admin_process', [OrderAdminController::class, 'store']);
    Route::resource('customer', CustomerController::class);
    Route::resource('pitch', PitchController::class);
    Route::resource('bill', BillController::class);
    Route::resource('time', TimeController::class);
    Route::resource('blog', BlogController::class);
    Route::resource('profile_admin', ProfileAdminController::class);
    Route::resource('change_password_admin', ChangePasswordAdminController::class);
    Route::get('/log_out_admin', [AdminLoginController::class, 'LogOut'])->name('log-out-admin');
});
