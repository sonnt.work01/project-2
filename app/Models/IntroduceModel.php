<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IntroduceModel extends Model
{
    use HasFactory;

    public $table = 'introduce';

    public $primaryKey = 'id';
}
