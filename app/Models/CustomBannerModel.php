<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomBannerModel extends Model
{
    use HasFactory;

    protected $table = "banner_custom";

    public $primaryKey = "id";
}
