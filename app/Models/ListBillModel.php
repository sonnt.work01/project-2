<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use NunoMaduro\Collision\Adapters\Phpunit\Style;

class ListBillModel extends Model
{
    use HasFactory;

    protected $table = 'listbill';

    public $primaryKey = 'id';
    public function getActiveNameAttribute()
    {
        if ($this->active == 1) {
            return 'Đang chờ duyệt';
        } elseif ($this->active == 2) {
            return 'Đã đặt';
        } elseif ($this->active == 3) {
            return 'Đã huỷ';
        }
        elseif ($this->active == 4) {
            return 'Đã thanh toán';
        }
    }
}
