<?php

namespace App\Mail;

use App\Models\CustomerModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ChangePassCustomer extends Mailable
{
    use Queueable, SerializesModels;


    protected $email;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email)
    {
        $this->email = $email;
    }

    public function randomPassword()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }



    public function build()
    {
        $pass = $this->randomPassword();
        // update trong bảng khách hàng 
        CustomerModel::where('email', $this->email)
            ->update(['password' => $pass]);
        // tạo giao diện để gửi giao diện về mail
        return $this->view('customer_manager.mail.success', [
            'pass' => $pass,
        ]);
    }
}
