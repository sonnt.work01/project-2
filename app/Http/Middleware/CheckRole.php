<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->session()->get('role') == 1) {
            return $next($request);
        } else {
            //nếu không tồn tại session
            return Redirect::route('login-admin')->with('error', 'Bạn không có quyền!');
        }
    }
}
