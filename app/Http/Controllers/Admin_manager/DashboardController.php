<?php

namespace App\Http\Controllers\Admin_manager;

use Exception;
use App\Models\AreaModel;
use App\Models\BillModel;
use App\Models\TimeModel;
use App\Models\AdminModel;
use App\Models\PitchModel;
use Illuminate\Http\Request;
use App\Models\ListBillModel;
use App\Http\Controllers\Controller;
use App\Models\ListAdminModel;
use App\Models\ListPitchModel;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $search = $request->get('search');
        $month = date('Y-m');
        if ($search == '') {
            $search = $month;
        }

        $admin_id = $request->session()->get('id');
        $admin = AdminModel::find($admin_id);
        if ($admin->role == 1) {
            $area_id = $request->get('area_id');
            $bill = ListBillModel::where('active', 1)->get();
            $listArea = AreaModel::where('del_flag', 1)->get();
            $listPitch = ListPitchModel::where('del_flag', 1)->where('area_id', $area_id)->get();
            // dd($listPitch);
        } else {
            $arr = array();
            $area = ListAdminModel::where('admin_id', $admin_id)->where('del_flag', 1)->get();
            foreach ($area as $value) {
                array_push($arr, $value->area_id);
            }
            $area_id = $arr;
            $listArea = ListAdminModel::where('del_flag', 1)->where('admin_id', $admin->id)->get();
            $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
            $listPitch = ListPitchModel::where('del_flag', 1)->where('area_id', $arr)->get();
        }
        $arr_area = array();
        $arr_pitch = array();
        $top_pitch = ListBillModel::where('area_id', $area_id)
            ->where('day', 'like', "%$search%")
            ->selectRaw('count(pitch_id) as so_hoa_don,pitch_name')
            ->groupBy('pitch_id')
            ->orderByRaw('count(pitch_id) DESC')
            ->limit(3)
            ->get();
        $top_time = ListBillModel::where('area_id', $area_id)
            ->where('day', 'like', "%$search%")
            ->selectRaw('count(time_id) as so_hoa_don,time_start, time_end')
            ->groupBy('time_id')
            ->orderByRaw('count(time_id) DESC')
            ->limit(3)
            ->get();
        $arr_doanh_thu_khu_vuc = array();
        foreach ($listArea as $value) {
            array_push($arr_area, $value->area_name);
            $doanh_thu_khu_vuc = ListBillModel::where('day', 'like', "%$search%")
                ->where('active', '=', 4)
                ->where('area_id', $value->id)
                ->sum('price') + ListBillModel::where('day', 'like', "%$search%")
                ->where('active', '=', 2)
                ->where('area_id', $value->id)
                ->sum('deposit');
            array_push($arr_doanh_thu_khu_vuc, $doanh_thu_khu_vuc);
        }
        // dd($doanh_thu_khu_vuc);
        // dd($arr_doanh_thu_khu_vuc);
        $arr_doanh_thu_san = array();
        foreach ($listPitch as $value) {
            array_push($arr_pitch, $value->pitch_name);
            $doanh_thu_san = ListBillModel::where('day', 'like', "%$search%")
                ->where('active', '=', 4)
                ->where('pitch_id', $value->id)
                ->sum('price') + ListBillModel::where('day', 'like', "%$search%")
                ->where('active', '=', 2)
                ->where('pitch_id', $value->id)
                ->sum('deposit');
            array_push($arr_doanh_thu_san, $doanh_thu_san);
        }
        // dd($area_id);
        return view('admin_manager.dashboard.dashboard', compact('arr_doanh_thu_khu_vuc', 'arr_area', 'arr_pitch', 'arr_doanh_thu_san'), [
            'search' => $search,
            'listArea' => $listArea,
            'admin' => $admin,
            'area_id' => $area_id,
            'bill' => $bill,
            'top_pitch' => $top_pitch,
            'top_time' => $top_time,
        ]);
    }
}
