<?php

namespace App\Http\Controllers\Admin_manager;

use Exception;
use App\Jobs\SendMail;
use App\Models\AdminModel;
use App\Jobs\SendMailAdmin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class AdminLoginController extends Controller
{
    public function index()
    {
        return view('admin_manager.auth.login');
    }
    public function LoginProcess(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');

        $validated = $request->validate([
            'email' => 'required|',
            'password' => 'required|',
        ], [
            'email.required' => "Email không được để trống!",
            'password.required' => "Mật khẩu không được để trống!",
        ]);

        try {
            $admin = AdminModel::where('email', "$email")->where('password', "$password")->where('del_flag', 1)->firstOrFail();
            $request->session()->put('id', $admin->id);
            $request->session()->put('role', $admin->role);
            return Redirect::route('dashboard');
        } catch (Exception $e) {
            return Redirect::route('login-admin')->with('error', 'Email hoặc mật khẩu không đúng!');
        }
    }
    public function LogOut(Request $request)
    {
        $request->session()->flush();
        return Redirect::route('login-admin');
    }

    public function ForgetPass()
    {
        return view('admin_manager.auth.forget_pass');
    }

    public function ProcessForgetPass(Request $request)
    {
        $emailAdmin = $request->get('email');
        // kiểm tra trên DB 
        $check_email = AdminModel::where('email', $emailAdmin)->count();
        if ($check_email != 0) {
            SendMailAdmin::dispatch($emailAdmin)->delay(now()->addSeconds(2));
            return Redirect::route('login-admin')->with('success', 'Mật khẩu mới đã được gửi đến Email của bạn!');
        } else {
            return Redirect::route('login-admin')->with('error', 'Lỗi, Email của bạn chưa được đăng ký với chúng tôi!');
        }
    }
}
