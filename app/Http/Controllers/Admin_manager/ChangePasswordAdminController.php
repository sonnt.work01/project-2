<?php

namespace App\Http\Controllers\Admin_manager;

use App\Models\BillModel;
use App\Models\AdminModel;
use Illuminate\Http\Request;
use App\Models\ListBillModel;
use App\Http\Controllers\Controller;

class ChangePasswordAdminController extends Controller
{
    public function index(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = AdminModel::find($id_admin);
        if($admin->role == 0){
            $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        }
        else{
            $bill = ListBillModel::where('active', 1)->get();
        }
        return view('admin_manager.change_password_admin.index', [
            'bill' => $bill,
            'admin' => $admin,
        ]);
    }
    public function update(Request $request, $id)
    {
        $old_password = $request->get('old_password');
        $new_password = $request->get('new_password');
        $repeat_new_password = $request->get('repeat_new_password');

        $count = AdminModel::where('id', $id)->where('password', $old_password)->count();
        if ($count == 0) {
            return redirect()->back()->with('error', 'Mật khẩu hiện tại không chính xác!');
        }

        $validated = $request->validate([
            'old_password' => 'required|',
            'new_password' => 'required|min:8',
        ], [
            'old_password.required' => "Mật khẩu hiện tại không được để trống!",
            'new_password.required' => "Mật khẩu mới không được để trống!",
            'new_password.min' => "Mật khẩu mới tối thiểu 8 ký tự!",
        ]);

        if ($new_password == $repeat_new_password) {
            $passAdmin = AdminModel::find($id);
            $passAdmin->password = $new_password;
            $passAdmin->save();
            return redirect()->back()->with('success', 'Đổi mật khẩu thành công!');
        } else {
            return redirect()->back()->with('error', 'Mật khẩu xác nhận không đúng!');
        }
    }
}
