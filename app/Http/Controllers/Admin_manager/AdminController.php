<?php

namespace App\Http\Controllers\Admin_manager;

use Exception;
use App\Models\AreaModel;
use App\Models\BillModel;
use App\Models\AdminModel;
use Illuminate\Http\Request;
use App\Models\ListBillModel;
use App\Models\ListAdminModel;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $del_flag = $request->get('def_flag');
        if ($del_flag == null) {
            $del_flag = 1;
        }
        $id_admin = $request->session()->get('id');
        $admin = AdminModel::find($id_admin);
        if ($admin->role == 0) {
            $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        } else {
            $bill = ListBillModel::where('active', 1)->get();
        }
        $listAdmin = AdminModel::where('role', 0)->where('del_flag', $del_flag)->get();

        return view('admin_manager.admin.index', [
            'listAdmin' => $listAdmin,
            'admin' => $admin,
            'bill' => $bill,
            'del_flag' => $del_flag,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = AdminModel::find($id_admin);
        if ($admin->role == 0) {
            $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        } else {
            $bill = ListBillModel::where('active', 1)->get();
        }
        $listArea = AreaModel::all();
        return view('admin_manager.admin.create', [
            'listArea' => $listArea,
            'admin' => $admin,
            'bill' => $bill,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|min:2|max:255',
            'email' => 'required|unique:admin',
            'password' => 'required|min:8|',
            'gender' => 'required|',
            'phone' => 'required|digits:10|regex:/(0)[0-9]{9}/',
            'date_birth' => 'required',
            'address' => 'required',
        ], [
            'name.required' => "Tên không được để trống!",
            'name.min' => "Tên có độ dài không được nhỏ hơn 2 ký tự!",
            'name.max' => "Tên có độ dài không được lớn hơn 255 ký tự!",
            'email.required' => "Email không được để trống!",
            'email.unique' => "Email đã tồn tại!",
            'password.required' => "Mật khẩu không được để trống!",
            'password.min' => "Mật khẩu tối đa 8 ký tự!",
            'gender.required' => "Giới tính không được để trống!",
            'phone.required' => "Số điện thoại không được để trống!",
            'phone.digits' => "Số điện thoại không hợp lệ, chỉ có 10 chữ số!",
            'phone.regex' => "Số điện thoại không hợp lệ!",
            'date_birth.required' => "Ngày sinh không được để trống!",
            'address.required' => "Địa chỉ không được để trống!",
        ]);

        $name = $request->get('name');
        $email = $request->get('email');
        $password = $request->get('password');
        $gender = $request->get('gender');
        $phone = $request->get('phone');
        $dateBirth = $request->get('date_birth');
        $address = $request->get('address');

        $admin = new AdminModel();
        $admin->name = $name;
        $admin->email = $email;
        $admin->password = $password;
        $admin->gender = $gender;
        $admin->phone = $phone;
        $admin->date_birth = $dateBirth;
        $admin->address = $address;
        $admin->role = 0;
        $admin->del_flag = 1;
        $admin->save();
        return redirect()->back()->with('success', 'Thêm nhân viên thành công!');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        try {
            $admin_edit = ListAdminModel::where('id', $id)->firstOrFail();
            $id_admin = $request->session()->get('id');
            $admin = AdminModel::find($id_admin);
            if ($admin->role == 0) {
                $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
            } else {
                $bill = ListBillModel::where('active', 1)->get();
            }
            // $admin_edit = ListAdminModel::find($id);
            $listArea = AreaModel::all();
            return view('admin_manager.admin.edit', [
                'admin' => $admin,
                'listArea' => $listArea,
                'admin_edit' => $admin_edit,
                'bill' => $bill,
            ]);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Lỗi!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admin = AdminModel::find($id);

        $validated = $request->validate([
            'name' => 'required|min:2|max:255',
            'phone' => 'required|digits:10|regex:/(0)[0-9]{9}/',
            'date_birth' => 'required',
            'address' => 'required',
            'del_flag' => 'required',
        ], [
            'phone.regex' => "Số điện thoại không hợp lệ!",
            'name.required' => "Tên không được để trống!",
            'name.min' => "Tên có độ dài không được nhỏ hơn 2 ký tự!",
            'name.max' => "Tên có độ dài không được lớn hơn 255 ký tự!",
            'phone.digits' => "Số điện thoại không hợp lệ, chỉ có 10 chữ số!",
            'phone.max' => "Số điện thoại tối đa 10 số!",
            'phone.numeric' => "Số điện thoại chỉ được nhập ký tự dạng số!",
            'date_birth.required' => "Ngày sinh không được để trống!",
            'address.required' => "Địa chỉ không được để trống!",
            'del_flag.required' => "Trạng thái hoạt động không được để trống!",
        ]);
        $admin->name = $request->get('name');
        $admin->date_birth = $request->get('date_birth');
        $admin->gender = $request->get('gender');
        $admin->phone = $request->get('phone');
        $admin->address = $request->get('address');
        $admin->del_flag = $request->get('del_flag');
        $admin->save();
        return Redirect::route('admin.index')->with('success', 'Cập nhật thông tin thành công!');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
