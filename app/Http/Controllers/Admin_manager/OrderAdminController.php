<?php

namespace App\Http\Controllers\Admin_manager;

use Pitch;
use Exception;
use App\Models\AreaModel;
use App\Models\BillModel;
use App\Models\TimeModel;
use App\Models\AdminModel;
use Illuminate\Http\Request;
use App\Models\CustomerModel;
use App\Models\ListBillModel;
use App\Models\IntroduceModel;
use App\Models\ListPitchModel;
use App\Http\Controllers\Controller;
use App\Models\PitchModel;
use Illuminate\Support\Facades\Redirect;

class OrderAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $area_id, $pitch_id, $search)
    {
        $array_time = array();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $today = date('Y-m-d');
        $listCustomer = CustomerModel::where('del_flag', 1)->get();
        $listArea = AreaModel::where('del_flag', 1)->get();
        $id_admin = $request->session()->get('id');
        $admin = AdminModel::find($id_admin);
        if ($admin->role == 0) {
            $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        } else {
            $bill = ListBillModel::where('active', 1)->get();
        }
        try {
            $area = AreaModel::where('id', "$area_id")->firstOrFail();
            $pitch = ListPitchModel::where('id', "$pitch_id")->where('area_id', "$area->id")->firstOrFail();
            if ($pitch->pitch_type == 1) {
                $listBill = ListBillModel::where('area_id', $area_id)
                    ->where('location', 1)
                    ->where('day', "$search")
                    ->where('active', '<>', "3")
                    ->where('active', '<>', "4")
                    ->select('time_id')->get();
                // dd($listBill);
            } elseif ($pitch->location == 1 && $pitch->pitch_type == 0) {
                $listBill = ListBillModel::where('area_id', $area_id)
                    ->where('pitch_id', $pitch_id)
                    ->where('day', "$search")
                    ->where('active', '<>', "3")
                    ->where('active', '<>', "4")
                    ->orwhere('pitch_type', 1)
                    ->where('area_id', $area_id)
                    ->where('day', "$search")
                    ->where('active', '<>', "3")
                    ->where('active', '<>', "4")
                    ->select('time_id')->get();
            } else {
                $listBill = ListBillModel::where('area_id', $area_id)
                    ->where('pitch_id', $pitch_id)
                    ->where('day', "$search")
                    ->where('active', '<>', "3")
                    ->where('active', '<>', "4")
                    ->select('time_id')->get();
            }
            foreach ($listBill as $value) {
                array_push($array_time, $value->time_id);
            }
            // dd($array_time);
            $listTime = TimeModel::where('del_flag', 1)->get();
            $empty_time_id = TimeModel::where('del_flag', 1)->whereNotIn('id', $array_time)->get();
            return view('admin_manager.order_admin.index', [
                'listCustomer' => $listCustomer,
                'pitch' => $pitch,
                'listBill' => $listBill,
                'search' => $search,
                'listTime' => $listTime,
                'area' => $area,
                'today' => $today,
                'empty_time_id' => $empty_time_id,
                'listArea' => $listArea,
                'listCustomer' => $listCustomer,
                'admin' => $admin,
                'bill' => $bill,
            ]);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Sân hoặc khu vực không hợp lệ!');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'customer_id' => 'required|',
            'pitch_id' => 'required|',
            'day' => 'required',
            'time_id' => 'required',
            'price' => 'required',
            'deposit' => 'required',
        ], [
            'customer_id.required' => "Tên khách hàng không được để trống!",
            'pitch_id.required' => "Sân không được để trống!",
            'day.required' => "NNgày không được để trống!",
            'time_id.required' => "khung thời gian không được để trống!",
            'price.required' => "Giá không được để trống!",
            'deposit.required' => "Tiền đặt cọc không được để trống!",
        ]);

        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $today = date('Y-m-d');

        $customer_id = $request->get('customer_id');
        $day = $request->get('day');
        $time_id = $request->get('time_id');
        $deposit = $request->get('deposit');
        $price = $request->get('price');
        $pitch_id = $request->get('pitch_id');
        $pitch = PitchModel::find($pitch_id);
        $customer_check = CustomerModel::where('id', $customer_id)->count();
        if ($customer_check == 0) {
            return redirect()->back()->with('error', 'Khách hàng không tồn tại!!!');
        }
        if ($time_id == 0) {
            return redirect()->back()->with('error', 'Sân đã kín lịch!!!');
        }
        if ($price != $pitch->price) {
            return redirect()->back()->with('error', 'Lỗi!!!');
        }
        if ($day < $today) {
            return redirect()->back()->with('error', 'Chọn ngày phải lớn hơn hoặc bằng ngày hôm nay!');
        }

        $count = ListBillModel::where('day', "$day")
            ->where('time_id', "$time_id")
            ->where('pitch_id', "$pitch_id")
            ->where('active', '<>', 3)
            ->count();
        if ($count != 0) {
            return redirect()->back()->with('error', 'Sân đã có người khác đặt trước!');
        }
        if ($pitch->pitch_type == 1) {
            $check = ListBillModel::where('day', "$day")
                ->where('time_id', "$time_id")
                ->where('location', 1)
                ->where('active', '<>', 3)
                ->where('pitch_id', "$pitch_id")
                ->count();
            if ($check != 0) {
                return redirect()->back()->with('error', 'Sân đã có người khác đặt trước!');
            }
        } elseif ($pitch->pitch_type == 0 && $pitch->location == 1) {
            $check = ListBillModel::where('day', "$day")
                ->where('time_id', "$time_id")
                ->where('location', 1)
                ->where('pitch_type', 1)
                ->where('active', '<>', 3)
                ->where('pitch_id', "$pitch_id")
                ->count();
            if ($check != 0) {
                return redirect()->back()->with('error', 'Sân đã có người khác đặt trước!');
            }
        }
        try {
            $pitch_check = PitchModel::where('id', "$pitch_id")->firstOrFail();
            $bill = new BillModel();
            $bill->customer_id = $customer_id;
            $bill->day = $day;
            $bill->time_id = $time_id;
            $bill->pitch_id = $pitch_id;
            $bill->active = 1;
            $bill->price = $price;
            $bill->deposit = $deposit;
            $bill->save(); //gio nghi check ben nay da, trung ngay gio, khu vuc với sân thi đặt lại
            return Redirect::route('pitch.index')->with('success', 'Đặt thành công!');
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Sân này không tồn tại!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
