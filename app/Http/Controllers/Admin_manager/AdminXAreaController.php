<?php

namespace App\Http\Controllers\Admin_manager;


use Exception;
use App\Models\AreaModel;
use App\Models\BillModel;
use App\Models\AdminModel;
use Illuminate\Http\Request;
use App\Models\ListBillModel;
use App\Models\ListAdminModel;
use App\Models\ListPitchModel;
use App\Http\Controllers\Controller;
use App\Models\AdminXAreaModel;
use Illuminate\Support\Facades\Redirect;

class AdminXAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = AdminModel::find($id_admin);
        $del_flag = $request->get('def_flag');

        if ($admin->role == 0) {
            $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        } else {
            $bill = ListBillModel::where('active', 1)->get();
        }

        if ($del_flag == null) {
            $del_flag = 1;
        }
        // $listArea = AreaModel::where('del_flag', $del_flag)->get();
        $listAdmin = ListAdminModel::where('role', 0)->where('del_flag', $del_flag)->get();
        return view('admin_manager.admin_x_area.index',  compact($listAdmin), [
            'listAdmin' => $listAdmin,
            'admin' => $admin,
            'bill' => $bill,
            'del_flag' => $del_flag,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = AdminModel::find($id_admin);
        if ($admin->role == 0) {
            $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        } else {
            $bill = ListBillModel::where('active', 1)->get();
        }
        $admin_id_arr = array();
        $area_id_arr = array();
        $list = AdminXAreaModel::where('del_flag', 1)->get();
        // dd($list);
        foreach ($list as $value) {
            array_push($admin_id_arr, $value->admin_id);
        }
        foreach ($list as $value) {
            array_push($area_id_arr, $value->area_id);
        }
        $listAdmin = AdminModel::where('role', 0)->where('del_flag', 1)->whereNotIn('id', $admin_id_arr)->get();
        $listArea = AreaModel::where('del_flag', 1)->whereNotIn('id', $area_id_arr)->get();
        // dd($listAdmin);
        return view('admin_manager.admin_x_area.create', [
            'bill' => $bill,
            'admin' => $admin,
            'listArea' => $listArea,
            'listAdmin' => $listAdmin,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'admin_id' => 'required|unique:admin_x_area',
            'area_id' => 'required|unique:admin_x_area',
        ], [
            'admin_id.required' => "Nhân viên không được để trống!",
            'area_id.required' => "Khu vực không được để trống!",
            'admin_id.unique' => "Nhân viên đã quản lý khu vực khác!",
            'area_id.unique' => "Khu vực đã có người quản lý!",
        ]);

        $admin_id = $request->get('admin_id');
        $count_admin = AdminModel::where('id', $admin_id)->where('del_flag', 1)->count();

        if ($count_admin == 0) {
            return redirect()->back()->with('error', 'Không tồn tại nhân viên này!');
        }

        $area_id = $request->get('area_id');
        $count_area = AreaModel::where('id', $area_id)->where('del_flag', 1)->count();

        if ($count_area == 0) {
            return redirect()->back()->with('error', 'Không tồn tại khu vực này!');
        }

        $adminxarea = new AdminXAreaModel();
        $adminxarea->admin_id = $admin_id;
        $adminxarea->area_id = $area_id;
        $adminxarea->del_flag = 1;
        $adminxarea->save();
        return redirect()->back()->with('success', 'Thêm thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        // try {
        $id_admin = $request->session()->get('id');
        $admin = AdminModel::find($id_admin);
        if ($admin->role == 0) {
            $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        } else {
            $bill = ListBillModel::where('active', 1)->get();
        }

        $admin_id_arr = array();
        $area_id_arr = array();
        $adminxarea = AdminXAreaModel::where('admin_id', $id)->firstOrFail();

        $list = AdminXAreaModel::where('del_flag', 1)->get();
        foreach ($list as $value) {
            array_push($admin_id_arr, $value->admin_id);
        }
        $area_id = $request->get('area_id');
        foreach ($list as $value) {
            array_push($area_id_arr, $value->area_id);
        }
        $listAdmin = AdminModel::where('role', 0)->where('del_flag', 1)->whereNotIn('id', $admin_id_arr)->get();
        $listArea = AreaModel::where('del_flag', 1)->whereNotIn('id', $area_id_arr)->get();
        // dd($list);
        return view('admin_manager.admin_x_area.edit', [
            'adminxarea' => $adminxarea,
            'bill' => $bill,
            'admin' => $admin,
            'listArea' => $listArea,
            'listAdmin' => $listAdmin,
        ]);
        // } catch (Exception $e) {
        //     return redirect()->back()->with('error', 'Lỗi');
        // }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'admin_id' => 'required|unique:admin_x_area',
            'area_id' => 'required|unique:admin_x_area',
        ], [
            'admin_id.required' => "Nhân viên không được để trống!",
            'area_id.required' => "Khu vực không được để trống!",
            'admin_id.unique' => "Nhân viên đã quản lý khu vực khác!",
            'area_id.unique' => "Khu vực đã có người quản lý!",
        ]);

        $adminxarea = AdminXAreaModel::find($id);

        $admin_id = $request->get('admin_id');
        $count_admin = AdminModel::where('id', $admin_id)->where('del_flag', 1)->count();

        if ($count_admin == 0) {
            return redirect()->back()->with('error', 'Không tồn tại nhân viên này!');
        }

        $area_id = $request->get('area_id');
        $count_area = AreaModel::where('id', $area_id)->where('del_flag', 1)->count();

        if ($count_area == 0) {
            return redirect()->back()->with('error', 'Không tồn tại khu vực này!');
        }

        $adminxarea->admin_id = $admin_id;
        $adminxarea->area_id = $area_id;
        $adminxarea->del_flag = 1;
        $adminxarea->save();
        return redirect()->back()->with('success', 'Thêm thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
