<?php

namespace App\Http\Controllers\Admin_manager;

use App\Models\BillModel;
use App\Models\AdminModel;
use Illuminate\Http\Request;
use App\Models\CustomerModel;
use App\Models\ListBillModel;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $del_flag = $request->get('def_flag');
        if ($del_flag == null) {
            $del_flag = 1;
        }
        $admin = AdminModel::find($id_admin);
        $search = $request->get('search');
        $listCustomer = CustomerModel::where('del_flag', $del_flag)->get();
        if($admin->role == 0){
            $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        }
        else{
            $bill = ListBillModel::where('active', 1)->get();
        }
        return view('admin_manager.customer.index', [
            'listCustomer' => $listCustomer,
            'search' => $search,
            'admin' => $admin,
            'bill' => $bill,
            'del_flag' => $del_flag,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
