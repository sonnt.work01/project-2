<?php

namespace App\Http\Controllers\Admin_manager;

use Exception;
use App\Models\AreaModel;
use App\Models\BillModel;
use App\Models\TimeModel;
use App\Models\AdminModel;
use App\Models\PitchModel;
use Illuminate\Http\Request;
use App\Models\ListBillModel;
use App\Models\ListPitchModel;
use App\Models\ListLocationModel;
use App\Http\Controllers\Controller;
use App\Models\AdminXAreaModel;
use App\Models\ListAdminModel;
use Illuminate\Support\Facades\Redirect;
use Pitch;

class PitchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = AdminModel::find($id_admin);
        $area_id = $request->get('area_id');

        $del_flag = $request->get('def_flag');
        if ($admin->role == 0) {
            $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        } else {
            $bill = ListBillModel::where('active', 1)->get();
        }
        $listTime = TimeModel::all();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $today = date('Y-m-d');
        $search = $request->get('search');
        if ($search == '') {
            $search = $today;
        }
        $listBill = ListBillModel::where('day', $search)->paginate();
        $del_flag = $request->get('def_flag');
        if ($del_flag == null) {
            $del_flag = 1;
        }
        if ($area_id == null) {
            $area = AreaModel::first();
            $area_id = $area->id;
        }
        $listArea = AreaModel::where('del_flag', 1)->get();
        if ($admin->role == 1) {
            $listPitch = ListPitchModel::where('area_id', $area_id)->where('del_flag', $del_flag)->get();
        } else {
            $listPitch = ListPitchModel::where('area_id', $area_id)->where('del_flag', $del_flag)->get();
        }
        // dd($listPitch);
        return view('admin_manager.pitch.index', [
            'listPitch' => $listPitch,
            'admin' => $admin,
            'area_id' => $area_id,
            'listArea' => $listArea,
            'bill' => $bill,
            'del_flag' => $del_flag,
            'listTime' => $listTime,
            'listBill' => $listBill,
            'search' => $search,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = AdminModel::find($id_admin);
        if ($admin->role == 1) {
            $listArea = ListAdminModel::where('del_flag', 1)->get();
        } else {
            $listArea = ListAdminModel::where('admin_id', $id_admin)->where('del_flag', 1)->get();
        }
        if ($admin->role == 0) {
            $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        } else {
            $bill = ListBillModel::where('active', 1)->get();
        }
        return view('admin_manager.pitch.create', [
            'listArea' => $listArea,
            'admin' => $admin,
            'bill' => $bill,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'pitch_name' => 'required',
            'area_id' => 'required|',
            'image' => 'required|mimes:jpeg,jpg,png',
            'pitch_type' => 'required|',
            'location' => 'required',
            'price' => 'required|numeric',
        ], [
            'pitch_name.required' => "Tên khu vực không được để trống!",
            'area_id.required' => "Khu vực không được để trống!",
            'image.required' => "Ảnh khu vực không được để trống!",
            'image.mimes' => "File ảnh không hợp lệ!",
            'pitch_type.required' => "Loại sân không được để trống!",
            'location.required' => "Vị trí không được để trống!",
            'price.required' => "Giá không được để trống!",
            'price.numeric' => "Giá chỉ được nhập ký tự dạng số!",
        ]);
        //image
        $image = $request->file('image');
        $newImageName = time() . '.' . $image->getClientOriginalExtension();
        $request->image->move(public_path('images'), $newImageName);
        //
        $pitchName = $request->get('pitch_name');

        $id_admin = $request->session()->get('id');
        $admin = AdminModel::find($id_admin);
        $areaId = $request->get('area_id');
        $area = AdminXAreaModel::where('admin_id', $id_admin)->where('area_id', $areaId)->where('del_flag', 1)->count();
        if ($area == 0 && $admin->role == 0) {
            return redirect()->back()->with('error', 'Khu vực không hợp lệ!');
        }
        $pitchType = $request->get('pitch_type');
        $price = $request->get('price');
        $location = $request->get('location');
        $count = ListPitchModel::where('area_id', $areaId)->where('location', 1)->where('del_flag', 1)->where('pitch_type', 0)->count();
        $count_pitchName = ListPitchModel::where('area_id', $areaId)->where('pitch_name', 'like', "%$pitchName%")->where('del_flag', 1)->count();
        if ($count_pitchName != 0) {
            return redirect()->back()->with('error', 'Tên sân đã tồn tại!');
        }
        if ($count == 4 && $pitchType == 0 && $location == 1) {
            return redirect()->back()->with('error', 'Đã đủ số sân ghép cho phép!');
        }
        if ($pitchType == 1 && $location != 1) {
            return redirect()->back()->with('error', 'Tạo sân 11 thì phải là sân ghép!');
        };
        $pitch = new PitchModel();
        $pitch->pitch_name = $pitchName;

        $pitch->area_id = $areaId;
        $pitch->location = $location;
        $pitch->pitch_type = $pitchType;
        $pitch->image_path = $newImageName;
        $pitch->price = $price;
        $pitch->del_flag = 1;
        $pitch->save();
        return redirect()->back()->with('success', 'Thêm sân thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        try {
            $pitch = PitchModel::where('id', $id)->firstOrFail();
            $id_admin = $request->session()->get('id');
            $admin = AdminModel::find($id_admin);

            if ($admin->role == 0) {
                $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
            } else {
                $bill = ListBillModel::where('active', 1)->get();
            }

            if ($admin->role == 1) {
                $listArea = AreaModel::where('del_flag', 1)->get();
            } else {
                $listArea = ListAdminModel::where('admin_id', $id_admin)
                    ->where('del_flag', 1)
                    ->get();
            }

            // dd($listArea);
            return view('admin_manager.pitch.edit', compact($pitch), [
                'pitch' => $pitch,
                'listArea' => $listArea,
                'admin' => $admin,
                'bill' => $bill,
            ]);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Lỗi');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'pitch_name' => 'required|',
            'area_id' => 'required|',
            'image' => 'mimes:jpeg,jpg,png',
            'pitch_type' => 'required|',
            'location' => 'required',
            'price' => 'required|numeric',
            'del_flag' => 'required|',
        ], [
            'pitch_name.required' => "Tên khu vực không được để trống!",
            'area_id.required' => "Khu vực không được để trống!",
            'image.mimes' => "File ảnh không hợp lệ!",
            'pitch_type.required' => "Loại sân không được để trống!",
            'location.required' => "Vị trí không được để trống!",
            'price.required' => "Giá không được để trống!",
            'price.numeric' => "Giá chỉ được nhập ký tự dạng số!",
            'del_flag.required' => "Trạng thái hoạt động không được để trống!",
        ]);

        $pitch = PitchModel::find($id);
        // image

        $image = $request->file('image');
        if ($image != '') {
            $image_name = time() . '.' . $image->getClientOriginalExtension();
            $request->image->move(public_path('images'), $image_name);
        } else {
            $image_name = $request->hidden_image;
        }
        $pitch_type = $request->get('pitch_type');
        $id_admin = $request->session()->get('id');
        $admin = AdminModel::find($id_admin);
        $areaId = $request->get('area_id');

        if ($admin->role == 1) {
            $area = AreaModel::where('id', $areaId)
                ->where('del_flag', 1)
                ->count();
        } else {
            $area = ListAdminModel::where('admin_id', $id_admin)
                ->where('area_id', $areaId)
                ->where('del_flag', 1)
                ->count();
        }

        if ($area == 0) {
            return redirect()->back()->with('error', 'Khu vực không hợp lệ!');
        }
        $location = $request->get('location');
        if ($location == 1 && $pitch->location == 0) {
            $count = ListPitchModel::where('area_id', $areaId)->where('location', 1)->where('pitch_type', 0)->where('del_flag', 1)->count();
            if ($count == 4 && $pitch_type == 0) {
                return redirect()->back()->with('error', 'Đã đủ số sân ghép cho phép!');
            }
        }
        if ($pitch_type == 1 && $location != 1) {
            return redirect()->back()->with('error', 'Tạo sân 11 thì phải là sân ghép!');
        };
        $pitchName = $request->get('pitch_name');
        $count_pitchName = ListPitchModel::where('area_id', $areaId)->where('pitch_name', 'like', "%$pitchName%")->where('del_flag', 1)->where('id', '<>', $id)->count();

        if ($count_pitchName != 0) {
            return redirect()->back()->with('error', 'Tên sân đã tồn tại!');
        }
        $check_delflag = ListBillModel::where('pitch_id', $id)
            ->where('active', '1')
            ->orwhere('pitch_id', $id)
            ->where('active', '2')
            ->count();
        $del_flag = $request->get('del_flag');
        if ($check_delflag != 0 && $del_flag == 0) {
            return redirect()->back()->with('error', 'Không thể đổi trạng thái vì có hoá đơn chưa xử lý xong!');
        }
        $pitch->pitch_name = $pitchName;
        $pitch->price = $request->get('price');
        $pitch->area_id = $areaId;
        $pitch->location = $location;
        $pitch->image_path = $image_name;
        $pitch->pitch_type = $pitch_type;
        $pitch->del_flag = $request->get('del_flag');
        $pitch->save();

        return Redirect::route('pitch.index')->with('success', 'Cập nhật sân thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
