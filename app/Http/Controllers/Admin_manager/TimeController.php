<?php

namespace App\Http\Controllers\Admin_manager;

use Exception;
use App\Models\BillModel;
use App\Models\TimeModel;
use App\Models\AdminModel;
use Illuminate\Http\Request;
use App\Models\ListBillModel;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

class TimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = AdminModel::find($id_admin);
        $del_flag = $request->get('def_flag');
        if ($del_flag == null) {
            $del_flag = 1;
        }
        if ($admin->role == 0) {
            $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        } else {
            $bill = ListBillModel::where('active', 1)->get();
        }
        $listTime = TimeModel::where('del_flag', $del_flag)->get();

        return view('admin_manager.time.index', [
            'listTime' => $listTime,
            'admin' => $admin,
            'bill' => $bill,
            'del_flag' => $del_flag,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = AdminModel::find($id_admin);
        if ($admin->role == 0) {
            $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        } else {
            $bill = ListBillModel::where('active', 1)->get();
        }
        return view('admin_manager.time.create', [
            'admin' => $admin,
            'bill' => $bill,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $array_timeStart = array();
        $validated = $request->validate([
            'time_start' => 'required',
            'time_end' => 'required',
        ], [
            'time_start.required' => "Thời gian bắt đầu không được để trống!",
            'time_end.required' => "Thời gian kết thúc không được để trống!",
        ]);
        $time_start = Carbon::parse($request->get('time_start'));
        $time_end = Carbon::parse($request->get('time_end'));
        if ($time_start->diffInMinutes($time_end) != 60 ||  $time_start->hour > $time_end->hour) {
            return redirect()->back()->with('error', 'Thời gian kết thúc phải lớn hơn thời gian bắt đầu 1 tiếng!');
        }
        $check_timeStart = TimeModel::where('del_flag', 1)->select('time_start')->get();
        foreach ($check_timeStart as $value) {
            array_push($array_timeStart, Carbon::parse($value->time_start)->hour);
        }
        if (in_array($time_start->hour, $array_timeStart)) {
            return redirect()->back()->with('error', 'Khung giờ bạn thêm trùng với khung giờ đã có!');
        }
        $time = new TimeModel();
        $time->time_start = $time_start;
        $time->time_end = $time_end;
        $time->del_flag = 1;
        $time->save();
        return redirect()->back()->with('success', 'Thêm khung giờ thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        try {
            $time = TimeModel::where('id', $id)->firstOrFail();
            $id_admin = $request->session()->get('id');
            $admin = AdminModel::find($id_admin);
            if ($admin->role == 0) {
                $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
            } else {
                $bill = ListBillModel::where('active', 1)->get();
            }
            // $time = TimeModel::find($id);
            return view('admin_manager.time.edit', [
                'admin' => $admin,
                'time' => $time,
                'bill' => $bill,
            ]);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Lỗi');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'time_start' => 'required|',
            'time_end' => 'required|',
            'del_flag' => 'required|',
        ], [
            'time_start.required' => "Thời gian bắt đầu không được để trống!",
            'time_end.required' => "Thời gian kết thúc không được để trống!",
            'del_flag.required' => "Trạng thái hoạt động không được để trống!",
        ]);
        $time = TimeModel::find($id);
        $check_delflag = ListBillModel::where('time_id',$id)->where('active','1')->orwhere('time_id',$id)->where('active','2')->count();
        $del_flag = $request->get('del_flag');
        if ($check_delflag != 0 && $del_flag == 0) {
            return redirect()->back()->with('error', 'Không thể đổi trạng thái vì có hoá đơn chưa xử lý xong!');
        }
        $time->del_flag = $request->get('del_flag');
        $time->save();
        return Redirect::route('time.index')->with('success', 'Cập nhật khung giờ thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
