<?php

namespace App\Http\Controllers\Admin_manager;

use App\Models\AreaModel;
use App\Models\AdminModel;
use App\Models\BillModel;
use App\Models\ListBillModel;
use App\Models\TimeModel;
use App\Models\PitchModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ListAdminModel;
use Illuminate\Support\Facades\Redirect;

class BillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // date_default_timezone_set('Asia/Ho_Chi_Minh');
        // $today = date('Y-m-d');
        $search = $request->get('search');
        // if ($search == '') {
        //     $search = $today;
        // }

        $area_id = $request->get('area_id');
        // if ($area_id == null) {
        //     $area = AreaModel::first();
        //     $area_id = $area->id;
        // }
        $bill_active = $request->get('bill_active');
        if ($bill_active == '') {
            $bill_active = 1;
        }

        $id_admin = $request->session()->get('id');
        $admin = AdminModel::find($id_admin);
        // if ($admin->role == 0) {
        //     $bill = ListBillModel::where('active', 1)->where()->get();
        // }
        // $bill = BillModel::where('active', 1)->get();
        if ($admin->role == 0) {
            $arr_area = array();
            $area = ListAdminModel::where('del_flag', 1)->where('admin_id', $admin->id)->get();
            foreach ($area as $value) {
                array_push($arr_area, $value->area_id);
            }
            $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
            if ($search == '') {
                $listBill = ListBillModel::where('admin_id', '=', $admin->id)
                    ->where('active', $bill_active)
                    ->get();
            } else {
                $listBill = ListBillModel::where('day', '=', "$search")
                    ->where('admin_id', '=', $admin->id)
                    ->where('active', $bill_active)
                    ->get();
            }
        } else {
            $area = AreaModel::where('del_flag', 1)->get();
            $bill = ListBillModel::where('active', 1)->get();
            if ($search == null) {
                $listBill = ListBillModel::where('active', $bill_active)
                    ->get();
            } else {
                $listBill = ListBillModel::where('day', '=', "$search")
                    ->where('area_id', $area_id)
                    ->where('active', $bill_active)
                    ->get();
            }
        }
        // dd($area_id);
        // dd($listBill);
        return view('admin_manager.bill.index', [
            'listBill' => $listBill,
            'search' => $search,
            'admin' => $admin,
            'area' => $area,
            'area_id' => $area_id,
            'bill_active' => $bill_active,
            'bill' => $bill,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id_admin = $request->session()->get('id');
        $admin = AdminModel::find($id_admin);
        $bill = ListBillModel::find($id);
        if ($bill->admin_id != $id_admin && $admin->role == 0) {
            return redirect()->back()->with('error', 'Lỗi thao tác!!!');
        }
        if ($bill->active == 1 && $bill->admin_id == $id_admin || $admin->role == 1 && $bill->active == 1) {
            $bill->active = 2;
            $bill->save();
            return Redirect::route('bill.index')->with('success', 'Hóa đơn đã duyệt thành công!');
        }
        if ($bill->active == 2 && $bill->admin_id == $id_admin || $admin->role == 1 && $bill->active == 2) {
            $bill->active = 4;
            $bill->save();
            return redirect()->back()->with('success', 'Hóa đơn đã được thanh toán!');
        } else {
            return redirect()->back()->with('error', 'Lỗi thao tác!!!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $id_admin = $request->session()->get('id');
        $admin = AdminModel::find($id_admin);
        $bill = ListBillModel::find($id);
        if ($bill->admin_id != $id_admin && $admin->role == 0) {
            return redirect()->back()->with('error', 'Lỗi thao tác!!!');
        }
        if ($bill->active == 1 && $bill->admin_id == $id_admin || $admin->role == 1 && $bill->active == 1) {
            $bill->active = 3;
            $bill->save();
            return Redirect::route('bill.index')->with('error', 'Hóa đơn đã được hủy!');
        } else {
            return Redirect::route('bill.index')->with('error', 'Lỗi thao tác!!!!');
        }
    }
}
