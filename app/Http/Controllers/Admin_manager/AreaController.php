<?php

namespace App\Http\Controllers\Admin_manager;

use Admin;
use Exception;
use App\Models\AreaModel;
use App\Models\BillModel;
use App\Models\AdminModel;
use Illuminate\Http\Request;
use App\Models\ListBillModel;
use App\Models\ListAdminModel;
use App\Models\ListPitchModel;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = AdminModel::find($id_admin);
        $del_flag = $request->get('def_flag');
        if ($admin->role == 0) {
            $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        } else {
            $bill = ListBillModel::where('active', 1)->get();
        }
        if ($del_flag == null) {
            $del_flag = 1;
        }
        // $listArea = AreaModel::where('del_flag', $del_flag)->get();
        $listArea = ListAdminModel::where('area_del_flag', $del_flag)->get();

        return view('admin_manager.area.index',  compact($listArea), [
            'listArea' => $listArea,
            'admin' => $admin,
            'bill' => $bill,
            'del_flag' => $del_flag,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $admin_id_arr = array();
        $id_admin = $request->session()->get('id');
        $admin = AdminModel::find($id_admin);
        $listArea = AreaModel::where('del_flag', 1)->get();
        foreach ($listArea as $value) {
            array_push($admin_id_arr, $value->admin_id);
        }
        $listAdmin = AdminModel::where('role', 0)->where('del_flag', 1)->whereNotIn('id', $admin_id_arr)->get();
        if ($admin->role == 0) {
            $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        } else {
            $bill = ListBillModel::where('active', 1)->get();
        }
        return view('admin_manager.area.create', [
            'admin' => $admin,
            'bill' => $bill,
            'listAdmin' => $listAdmin,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'area_name' => 'required',
            'admin_id' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png',
            'diagram' => 'required|mimes:jpeg,jpg,png',
            'area_address' => 'required',
        ], [
            'area_name.required' => "Tên khu vực không được để trống!",
            'admin_id.required' => "Người quản lý không được để trống!",
            'image.required' => "Ảnh khu vực không được để trống!",
            'image.mimes' => "File ảnh không hợp lệ!",
            'diagram.required' => "Ảnh sơ đồ không được để trống!",
            'diagram.mimes' => "File sơ đồ không hợp lệ!",
            'area_address.required' => "Địa chỉ không được để trống!",
        ]);
        //image
        $image = $request->file('image');
        $newImageName = time() . '.' . $image->getClientOriginalExtension();
        $request->image->move(public_path('images'), $newImageName);
        //diagram
        $diagram = $request->file('diagram');
        $newDiagramName = time() . '.' . $diagram->getClientOriginalExtension();
        $request->diagram->move(public_path('images'), $newDiagramName);
        //
        $areaName = $request->get('area_name');
        $areaAddress = $request->get('area_address');
        $count_areaName = AreaModel::where('area_name', 'like', "%$areaName%")->where('del_flag', 1)->count();

        if ($count_areaName != 0) {
            return redirect()->back()->with('error', 'Tên khu vực đã tồn tại!');
        }
        $area = new AreaModel();
        $area->area_name = $areaName;
        $area->image_path = $newImageName;
        $area->diagram = $newDiagramName;
        $area->area_address = $areaAddress;
        $area->del_flag = 1;
        $area->save();
        return redirect()->back()->with('success', 'Thêm khu vực thành công!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        try {
            $area = AreaModel::where('id', $id)->firstOrFail();
            $id_admin = $request->session()->get('id');
            $admin = AdminModel::find($id_admin);
            if ($admin->role == 0) {
                $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
            } else {
                $bill = ListBillModel::where('active', 1)->get();
            }

            return view('admin_manager.area.edit', compact($area), [
                'area' => $area,
                'admin' => $admin,
                'bill' => $bill,
            ]);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Lỗi');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'area_name' => 'required',
            // 'image' => 'required',
            // 'diagram' => 'required',
            'area_address' => 'required',
            'del_flag' => 'required',
        ], [
            'area_name.required' => "Tên khu vực không được để trống!",
            // 'image.required' => "Ảnh khu vực không được để trống",
            // 'diagram.required' => "Ảnh sơ đồ không được để trống",
            'area_address.required' => "Địa chỉ không được để trống!",
            'del_flag.required' => "Trạng thái hoạt động không được để trống!",
        ]);

        $area = AreaModel::find($id);

        $image = $request->file('image');
        $diagram = $request->file('diagram');
        if ($image != '') {
            $image_name = time() . '.' . $image->getClientOriginalExtension();
            $request->image->move(public_path('images'), $image_name);
        } else {
            $image_name = $request->hidden_image;
        }
        if ($diagram != '') {
            $diagram_name = time() . '.' . $diagram->getClientOriginalExtension();
            $request->diagram->move(public_path('images'), $diagram_name);
        } else {
            $diagram_name = $request->hidden_diagram;
        }

        $areaName = $request->get('area_name');
        $count_areaName = AreaModel::where('area_name', 'like', "%$areaName%")->where('del_flag', 1)->where('id', '<>', $id)->count();

        if ($count_areaName != 0) {
            return redirect()->back()->with('error', 'Tên khu vực đã tồn tại!');
        }
        $check_delflag = ListBillModel::where('area_id', $id)->where('active', '1')->orwhere('area_id', $id)->where('active', '2')->count();
        $del_flag = $request->get('del_flag');
        if ($check_delflag != 0 && $del_flag == 0) {
            return redirect()->back()->with('error', 'Không thể đổi trạng thái vì có hoá đơn chưa xử lý xong!');
        }

        $area->area_name = $areaName;
        $area->area_address = $request->get('area_address');
        $area->del_flag = $del_flag;
        $area->image_path = $image_name;
        $area->diagram = $diagram_name;
        $area->save();
        return Redirect::route('area.index')->with('success', 'Cập nhật khu vực thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AreaModel::find($id)->delete();
        return Redirect::route('area.index');
    }
}
