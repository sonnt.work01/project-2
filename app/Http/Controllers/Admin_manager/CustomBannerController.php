<?php

namespace App\Http\Controllers\Admin_manager;

use Exception;
use App\Models\AreaModel;
use App\Models\AdminModel;
use Illuminate\Http\Request;
use App\Models\ListBillModel;
use App\Models\ListAdminModel;
use App\Models\CustomBannerModel;
use App\Http\Controllers\Controller;
use App\Models\CustomerModel;
use CustomBanner;
use Illuminate\Support\Facades\Redirect;

class CustomBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = AdminModel::find($id_admin);
        $listBanner = CustomBannerModel::all();
        $del_flag = $request->get('def_flag');
        if ($admin->role == 0) {
            $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        } else {
            $bill = ListBillModel::where('active', 1)->get();
        }
        if ($del_flag == null) {
            $del_flag = 1;
        }
        return view('admin_manager.custom_banner.index', [
            'listBanner' => $listBanner,
            'admin' => $admin,
            'bill' => $bill,
            'del_flag' => $del_flag,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = AdminModel::find($id_admin);
        $listAdmin = AdminModel::where('role', 0)->where('del_flag', 1)->get();
        if ($admin->role == 0) {
            $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        } else {
            $bill = ListBillModel::where('active', 1)->get();
        }
        return view('admin_manager.custom_banner.create', [
            'admin' => $admin,
            'bill' => $bill,
            'listAdmin' => $listAdmin,
        ]);
    }

    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  */
    public function store(Request $request)
    {
        //image
        $image = $request->file('image');
        $newImageName = time() . '.' . $image->getClientOriginalExtension();
        $request->image->move(public_path('images'), $newImageName);

        $location = $request->get('location');
        // dd($location);   
        // $validated = $request->validate([
        //     'image' => 'required|mimes:jpeg,jpg,png',
        //     'location' => 'required|',
        // ], [
        //     'image.required' => "Ảnh khu vực không được để trống!",
        //     'image.mimes' => "File ảnh không hợp lệ!",
        //     'location.required' => "Vị trí không được để trống!",
        // ]);
        $count_location = CustomBannerModel::where('location', $location)->count();

        if ($count_location != 0) {
            return redirect()->back()->with('error', 'Lỗi!!! Vị trí bạn chọn đã có ảnh banner!');
        }
        $banner = new CustomBannerModel();
        $banner->image_path = $newImageName;
        $banner->location = $location;
        $banner->save();
        return redirect()->back()->with('success', 'Thêm banner thành công!');
    }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function show($id)
    // {
    //     //
    // }

    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    public function edit(Request $request, $id)
    {
        try {
            $banner = CustomBannerModel::where('id', $id)->firstOrFail();
            $id_admin = $request->session()->get('id');
            $admin = AdminModel::find($id_admin);
            if ($admin->role == 0) {
                $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
            } else {
                $bill = ListBillModel::where('active', 1)->get();
            }
            $location = $request->get('location');
            $listBanner = CustomBannerModel::where('location', '=', $location)->get();
            return view('admin_manager.custom_banner.edit', [
                'admin' => $admin,
                'bill' => $bill,
                'listBanner' => $listBanner,
                'banner' => $banner,
            ]);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Lỗi');
        }
    }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    public function update(Request $request, $id)
    {
        $banner = CustomBannerModel::find($id);

        $image = $request->file('image');
        if ($image != '') {
            $image_name = time() . '.' . $image->getClientOriginalExtension();
            $request->image->move(public_path('images'), $image_name);
        } else {
            $image_name = $request->hidden_image;
        }

        $banner->image_path = $image_name;
        $banner->save();
        return Redirect::route('custom_banner.index')->with('success', 'Cập nhật banner thành công!');
    }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function destroy($id)
    // {
    //     AreaModel::find($id)->delete();
    //     return Redirect::route('area.index');
    // }
}
