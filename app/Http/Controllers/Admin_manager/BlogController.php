<?php

namespace App\Http\Controllers\Admin_manager;

use Blog;
use Exception;
use App\Models\BillModel;
use App\Models\BlogModel;
use App\Models\AdminModel;
use Illuminate\Http\Request;
use App\Models\ListBillModel;
use App\Models\ListBlogModel;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id_admin = $request->session()->get('id');
        $admin = AdminModel::find($id_admin);
        $del_flag = $request->get('def_flag');
        if ($admin->role == 0) {
            $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        } else {
            $bill = ListBillModel::where('active', 1)->get();
        }

        $del_flag = $request->get('def_flag');
        if ($del_flag == null) {
            $del_flag = 1;
        }

        $search = $request->get('search');
        $listBlog = ListBlogModel::where('del_flag', $del_flag)->get();

        return view('admin_manager.blog.index',  compact($listBlog), [
            'listBlog' => $listBlog,
            'search' => $search,
            'admin' => $admin,
            'bill' => $bill,
            'del_flag' => $del_flag,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $today = date('Y-m-d');
        $id_admin = $request->session()->get('id');
        $admin = AdminModel::find($id_admin);
        if ($admin->role == 0) {
            $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
        } else {
            $bill = ListBillModel::where('active', 1)->get();
        }
        return view('admin_manager.blog.create', [
            'admin' => $admin,
            'today' => $today,
            'bill' => $bill,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required|max:255',
            'image' => 'required|mimes:jpeg,jpg,png',
            'admin_id' => 'required',
            'content' => 'required',
            'date_submitted' => 'required',
        ], [
            'title.required' => "Tên bài viết không được để trống!",
            'title.max' => "Tên bài viết tối đa 255 ký tự!",
            'admin_id.required' => "Người đăng không được lớn hớn 255 ký tự!",
            'image.required' => "Bạn chưa chọn ảnh!",
            'image.mimes' => "Ảnh không hợp lệ!",
            'content.required' => "Nội dung không được để trống!",
            'date_submitted.required' => "Bạn chưa chọn ngày!",
        ]);
        //image
        // cách 1
        $image = $request->file('image');
        $newImageName = time() . '.' . $image->getClientOriginalExtension();
        $request->image->move(public_path('images'), $newImageName);
        //cách 2
        // if ($request->hasFile('image')) {
        //     $file = $request->file('image');
        //     $extension = $file->getClientOriginalExtension();
        //     $fileName = time() . '.' . $extension;
        //     $file->move(public_path('images'), $fileName);
        // }
        //
        $title = $request->get('title');
        $admin_id = $request->get('admin_id');
        $content = $request->get('content');
        $date_submitted = $request->get('date_submitted');

        $blog = new BlogModel();
        $blog->title = $title;
        $blog->admin_id = $admin_id;
        $blog->image_path = $newImageName;
        $blog->content = $content;
        $blog->date_submitted = $date_submitted;
        $blog->del_flag = 1;
        $blog->save();
        return redirect()->back()->with('success', 'Thêm bài viết thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        try {
            $blog = BlogModel::where('id', $id)->firstOrFail();
            $id_admin = $request->session()->get('id');
            $admin = AdminModel::find($id_admin);
            if ($admin->role == 0) {
                $bill = ListBillModel::where('admin_id', '=', $admin->id)->where('active', 1)->get();
            } else {
                $bill = ListBillModel::where('active', 1)->get();
            }
            // $blog = BlogModel::find($id);
            return view('admin_manager.blog.edit', compact($blog), [
                'blog' => $blog,
                'admin' => $admin,
                'bill' => $bill,
            ]);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Lỗi');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'title' => 'required|max:255',
            // 'image' => 'required|mimes:jpeg,jpg,png',
            'admin_id' => 'required',
            'content' => 'required',
            'date_submitted' => 'required',
            'del_flag' => 'required',
        ], [
            'title.required' => "Tên bài viết không được để trống!",
            'title.max' => "Tên bài viết tối đa 255 ký tự!",
            'admin_id.required' => "Người đăng không được lớn hớn 255 ký tự!",
            // 'image.required' => "Bạn chưa chọn ảnh!",
            // 'image.mimes' => "File ảnh không hợp lệ!",
            'content.required' => "Nội dung không được để trống!",
            'date_submitted.required' => "Bạn chưa chọn ngày!",
            'del_flag.required' => "Trạng thái hoạt động không được để trống!",
        ]);

        $blog = BlogModel::find($id);
        $image = $request->file('image');

        if ($image != '') {
            $image_name = time() . '.' . $image->getClientOriginalExtension();
            $request->image->move(public_path('images'), $image_name);
        } else {
            $image_name = $request->hidden_image;
        }
        $blog->title = $request->get('title');
        $blog->admin_id = $request->get('admin_id');
        $blog->date_submitted = $request->get('date_submitted');
        $blog->content = $request->get('content');
        $blog->del_flag = $request->get('del_flag');
        $blog->image_path = $image_name;
        $blog->save();
        return Redirect::route('blog.index')->with('success', 'Cập nhật bài viết thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
