<?php

namespace App\Http\Controllers\Customer_manager;

use App\Models\AreaModel;
use Illuminate\Http\Request;
use App\Models\CustomerModel;
use App\Models\IntroduceModel;
use App\Models\ListAdminModel;
use App\Models\CustomBannerModel;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $contact = ListAdminModel::where('del_flag', 1)->get();
        $listArea = AreaModel::all();
        $introduce = IntroduceModel::where('del_flag', 1)->get();
        $id_customer = $request->session()->get('id_customer');
        if (isset($id_customer)) {
            $customer = CustomerModel::find($id_customer);
        } else {
            $customer = '';
        }
        return view(
            'customer_manager.contact_us.index',
            [
                'contact' => $contact,
                'introduce' => $introduce,
                'customer' => $customer,
                'listArea' => $listArea,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
