<?php

namespace App\Http\Controllers\Customer_manager;

use Exception;
use App\Models\AreaModel;
use Illuminate\Http\Request;
use App\Models\CustomerModel;
use App\Models\IntroduceModel;
use App\Http\Controllers\Controller;
use App\Models\CustomBannerModel;

class ChangePasswordController extends Controller
{
    public function index(Request $request)
    {
        $listArea = AreaModel::all();
        $id_customer = $request->session()->get('id_customer');
        $customer = CustomerModel::find($id_customer);
        $introduce = IntroduceModel::all();

        return view('customer_manager.change_password.index', [
            'customer' => $customer,
            'introduce' => $introduce,
            'listArea' => $listArea,
        ]);
    }

    public function update(Request $request, $id)
    {

        $old_password = $request->get('old_password');
        $new_password = $request->get('new_password');
        $repeat_new_password = $request->get('repeat_new_password');

        $count = CustomerModel::where('id', $id)->where('password', $old_password)->count();
        if ($count == 0) {
            return redirect()->back()->with('error', 'Mật khẩu hiện tại không chính xác!');
        }

        $validated = $request->validate([
            'old_password' => 'required|',
            'new_password' => 'required|min:8',
        ], [
            'old_password.required' => "Mật khẩu hiện tại không được để trống!",
            'new_password.required' => "Mật khẩu mới không được để trống!",
            'new_password.min' => "Mật khẩu mới tối thiểu 8 ký tự!",
        ]);


        if ($new_password == $repeat_new_password) {
            $passCustomer = CustomerModel::find($id);
            $passCustomer->password = $new_password;
            $passCustomer->save();
            return redirect()->back()->with('success', 'Đổi mật khẩu thành công!');
        } else {
            return redirect()->back()->with('error', 'Mật khẩu xác nhận không đúng!');
        }
    }
}
