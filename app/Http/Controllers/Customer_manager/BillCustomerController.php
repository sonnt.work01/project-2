<?php

namespace App\Http\Controllers\Customer_manager;

use App\Models\AreaModel;
use App\Models\BillModel;
use Illuminate\Http\Request;
use App\Models\CustomerModel;
use App\Models\ListBillModel;
use App\Models\IntroduceModel;
use App\Models\CustomBannerModel;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class BillCustomerController extends Controller
{
    public function index(Request $request)
    {
        $bill_active = $request->get('bill_active');
        if ($bill_active == '') {
            $bill_active = 1;
        }
        $introduce = IntroduceModel::where('del_flag', 1)->get();
        $listArea = AreaModel::all();
        $id_customer = $request->session()->get('id_customer');
        $customer = CustomerModel::find($id_customer);
        $listBill = ListBillModel::where('customer_id', $id_customer)->where('active', $bill_active)->get();
        $listBill->day = date('d-m-Y');
        return view('customer_manager.bill_customer.index', [
            'customer' => $customer,
            'listBill' => $listBill,
            'introduce' => $introduce,
            'listArea' => $listArea,
            'bill_active' => $bill_active,
        ]);
    }
    public function destroy_process(Request $request, $bill_id)
    {
        $id_customer = $request->session()->get('id_customer');
        $customer = CustomerModel::find($id_customer);
        $bill = BillModel::find($bill_id);
        if ($bill->active == 1 && $bill->customer_id == $id_customer) {
            $bill->active = 3;
            $bill->save();
            return Redirect::route('bill-page')->with('success', 'Hóa đơn đã được hủy!');
        } else {
            return Redirect::route('bill-page')->with('error', 'Lỗi thao tác!!!');
        }
    }
}
