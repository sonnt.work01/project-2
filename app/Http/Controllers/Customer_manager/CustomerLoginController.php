<?php

namespace App\Http\Controllers\Customer_manager;

use Exception;
use Illuminate\Http\Request;
use App\Models\CustomerModel;
use App\Jobs\SendMailCustomer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class CustomerLoginController extends Controller
{
    public function index()
    {
        return view('customer_manager.auth.login');
    }

    public function LoginProcess(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');
        $validated = $request->validate([
            'email' => 'required|',
            'password' => 'required|',
        ], [
            'email.required' => "Email không được để trống!",
            'password.required' => "Mật khẩu không được để trống!",
        ]);
        try {
            $admin = CustomerModel::where('email', "$email")->where('password', "$password")->where('del_flag', 1)->firstOrFail();
            $request->session()->put('id_customer', $admin->id);
            return Redirect::route('home');
        } catch (Exception $e) {
            return Redirect::route('login')->with('error', 'Email hoặc mật khẩu không đúng!');
        }
    }
    public function LogOut(Request $request)
    {
        $request->session()->flush();
        return Redirect::route('home');
    }

    public function ForgetPass()
    {
        return view('customer_manager.auth.forget_pass');
    }

    public function ProcessForgetPass(Request $request)
    {
        $emailCus = $request->get('email');
        // kiểm tra trên DB 
        $check_email = CustomerModel::where('email', $emailCus)->count();
        if ($check_email != 0) {
            SendMailCustomer::dispatch($emailCus)->delay(now()->addSeconds(2));
            return Redirect::route('login')->with('success', 'Mật khẩu mới đã được gửi đến Email của bạn!');
        } else {
            return Redirect::route('login')->with('error', 'Lỗi, Email của bạn chưa được đăng ký với chúng tôi!');
        }
    }
}
