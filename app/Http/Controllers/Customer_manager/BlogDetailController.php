<?php

namespace App\Http\Controllers\Customer_manager;

use Exception;
use App\Models\AreaModel;
use Illuminate\Http\Request;
use App\Models\CustomerModel;
use App\Models\ListBlogModel;
use App\Models\IntroduceModel;
use App\Models\CustomBannerModel;
use App\Http\Controllers\Controller;

class BlogDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {
        try {
            $listArea = AreaModel::all();
            $blog = ListBlogModel::find($id);
            $listBlog = ListBlogModel::inRandomOrder()->limit(3)->get();
            $id_customer = $request->session()->get('id_customer');
            if (isset($id_customer)) {
                $customer = CustomerModel::find($id_customer);
            } else {
                $customer = '';
            }
            $introduce = IntroduceModel::where('del_flag', 1)->get();
            return view('customer_manager.blog_detail.index', [
                'customer' => $customer,
                'blog' => $blog,
                'introduce' => $introduce,
                'listBlog' => $listBlog,
                'listArea' => $listArea,
            ]);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Bài viết không hợp lệ!');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
