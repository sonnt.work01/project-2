<?php

namespace App\Http\Controllers\Customer_manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CustomerModel;
use Illuminate\Support\Facades\Redirect;

class SignupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('customer_manager.auth.signup');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|min:2|max:255',
            'phone' => 'required|digits:10|regex:/(0)[0-9]{9}/',
            'email' => 'required|regex:/(.*)@gmail\.com/i|unique:customer',
            'password' => 'required|min:8',
            'date_birth' => 'required',
            'gender' => 'required',
        ], [
            'name.required' => "Tên không được để trống!",
            'name.min' => "Tên có độ dài không được nhỏ hơn 2 ký tự!",
            'name.max' => "Tên có độ dài không được lớn hơn 255 ký tự!",
            'email.required' => "Email không được để trống!",
            'email.regex' => "Email không hợp lệ!",
            'email.unique' => "Email đã tồn tại!",
            'phone.required' => "Số điện thoại không được để trống!",
            'phone.digits' => "Số điện thoại không hợp lệ, chỉ có 10 chữ số!",
            'phone.regex' => "Số điện thoại không hợp lệ!",
            'password.required' => "Mật khẩu không được để trống!",
            'password.min' => "Mật khẩu tối thiểu 8 ký tự!",
            'date_birth.required' => "Ngày sinh không được để trống!",
            'gender.required' => "Giới tính không được để trống!",
        ]);
        $name = $request->get('name');
        $email = $request->get('email');
        $password = $request->get('password');
        $date_birth = $request->get('date_birth');
        $gender = $request->get('gender');
        $phone = $request->get('phone');

        $customer = new CustomerModel();
        $customer->name = $name;
        $customer->email = $email;
        $customer->password = $password;
        $customer->date_birth = $date_birth;
        $customer->phone = $phone;
        $customer->gender = $gender;
        $customer->del_flag = 1;
        $customer->save();
        return Redirect::route('login')->with('success', 'Đăng ký thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
