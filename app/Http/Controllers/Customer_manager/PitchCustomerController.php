<?php

namespace App\Http\Controllers\Customer_manager;

use Exception;
use App\Models\AreaModel;
use App\Models\TimeModel;
use App\Models\PitchModel;
use Illuminate\Http\Request;
use App\Models\CustomerModel;
use App\Models\ListBillModel;
use App\Models\IntroduceModel;
use App\Models\ListPitchModel;
use App\Models\CustomBannerModel;
use App\Http\Controllers\Controller;

class PitchCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {
        try {
            $check = AreaModel::where('id', $id)->firstOrFail();

            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $today = date('Y-m-d');
            $search = $request->get('search');
            if ($search == '') {
                $search = $today;
            }

            $id_customer = $request->session()->get('id_customer');
            if (isset($id_customer)) {
                $customer = CustomerModel::find($id_customer);
            } else {
                $customer = '';
            }

            $introduce = IntroduceModel::where('del_flag', 1)->get();

            $area = AreaModel::find($id);
            $listPitch = ListPitchModel::where('area_id', $id)->where('del_flag', 1)->paginate();
            $listArea = AreaModel::where('del_flag', 1)->get();
            $listBill = ListBillModel::where('day', $search)->paginate();
            $listTime = TimeModel::where('del_flag', 1)->get();

            return view('customer_manager.pitch_customer.index', compact($listPitch), [
                'listPitch' => $listPitch,
                'area' => $area,
                'listArea' => $listArea,
                'listBill' => $listBill,
                'search' => $search,
                'listTime' => $listTime,
                'customer' => $customer,
                'introduce' => $introduce,
                'today' => $today,
            ]);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Khu vực không hợp lệ!');
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
