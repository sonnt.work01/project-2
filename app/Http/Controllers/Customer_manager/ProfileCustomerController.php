<?php

namespace App\Http\Controllers\Customer_manager;

use Exception;
use App\Models\AreaModel;
use Illuminate\Http\Request;
use App\Models\CustomerModel;
use App\Models\IntroduceModel;
use App\Models\CustomBannerModel;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class ProfileCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id_customer = $request->session()->get('id_customer');
        $customer = CustomerModel::find($id_customer);

        $introduce = IntroduceModel::where('del_flag', 1)->get();
        $listArea = AreaModel::all();
        return view('customer_manager.profile_customer.index', [
            'customer' => $customer,
            'introduce' => $introduce,
            'listArea' => $listArea,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $listArea = AreaModel::all();
        $id_customer = $request->session()->get('id_customer');
        if ($id_customer != $id) {
            return redirect()->back()->with('error', 'Lỗi!');
        }
        $customer = CustomerModel::find($id);
        // $customer_edit = CustomerModel::find($id);
        $introduce = IntroduceModel::where('del_flag', 1)->get();
        return view('customer_manager.profile_customer.edit', [
            // 'customer_edit' => $customer_edit,
            'customer' => $customer,
            'introduce' => $introduce,
            'listArea' => $listArea,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id_customer = $request->session()->get('id_customer');
        $customer = CustomerModel::find($id_customer);

        $validated = $request->validate([
            'name' => 'required|min:2|max:255',
            'phone' => 'required|digits:10|regex:/(0)[0-9]{9}/',
            'date_birth' => 'required',
            'gender' => 'required',
        ], [
            'name.required' => "Tên không được để trống!",
            'name.min' => "Tên có độ dài không được nhỏ hơn 2 ký tự!",
            'name.max' => "Tên có độ dài không được lớn hơn 255 ký tự!",
            'phone.required' => "Số điện thoại không được để trống!",
            'phone.digits' => "Số điện thoại không hợp lệ, chỉ có 10 chữ số!",
            'phone.regex' => "Số điện thoại không hợp lệ!",
            'date_birth.required' => "Ngày sinh không được để trống!",
            'gender.required' => "Giới tính không được để trống!",
        ]);

        $customer->name = $request->get('name');
        $customer->date_birth = $request->get('date_birth');
        $customer->gender = $request->get('gender');
        $customer->phone = $request->get('phone');
        $customer->save();
        return Redirect::route('profile_customer.index')->with('success', "Cập nhật thông tin thành công!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
