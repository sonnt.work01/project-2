<?php

namespace App\Http\Controllers;

use App\Models\AreaModel;
use App\Models\BillModel;
use App\Models\CustomBannerModel;
use App\Models\TimeModel;
use App\Models\PitchModel;
use Illuminate\Http\Request;
use App\Models\CustomerModel;
use App\Models\IntroduceModel;
use App\Models\ListBlogModel;
use App\Models\ListPitchModel;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $listBlog = ListBlogModel::where('del_flag', 1)->orderBy('id', 'desc')->paginate(3);

        $id_customer = $request->session()->get('id_customer');
        if (isset($id_customer)) {
            $customer = CustomerModel::find($id_customer);
        } else {
            $customer = '';
        }
        $introduce = IntroduceModel::all();

        // $search = $request->get('search');
        $listArea = AreaModel::where('del_flag', 1)->get();
        // $listPitch = PitchModel::where('del_flag', 1)->get();
        // $listBill = BillModel::all();
        // $listTime = TimeModel::where('del_flag', 1)->get();

        return view('home', [
            'listArea' => $listArea,
            'customer' => $customer,
            'listBlog' => $listBlog,
            'introduce' => $introduce,
        ]);
    }
}
