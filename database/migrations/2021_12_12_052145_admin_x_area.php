<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AdminXArea extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_x_area', function (Blueprint $table) {

            $table->unsignedInteger('admin_id');
            $table->unsignedInteger('area_id');
            $table->foreign('admin_id')->references('id')->on('admin');
            $table->foreign('area_id')->references('id')->on('area');
            $table->boolean('del_flag');

            $table->timestamps(); // tu dong tao 2 cot là create_at va update_at
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_x_area');
    }
}
