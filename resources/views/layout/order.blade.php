<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/png" href="{{ asset('assets') }}/img/football-icon.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>BKSport Đặt sân</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- CSS Files -->
    <link href="{{ asset('assets') }}/css/bootstrap.min.css" rel="stylesheet" />
    <link href="{{ asset('assets') }}/css/material-kit.css?v=1.2.1" rel="stylesheet" />

    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{ asset('assets') }}/assets-for-demo/vertical-nav.css" rel="stylesheet" />
    <link href="{{ asset('assets') }}/assets-for-demo/demo.css" rel="stylesheet" />
</head>

<body class="ecommerce-page">
    {{-- navbar --}}
    <nav class="navbar navbar-primary navbar-fixed-top navbar-color-on" id="sectionsNav">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">BÓNG ĐÁ BÁCH KHOA SPORT</a>
            </div>

            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="/about-us.html">
                            <i class="material-icons">apps</i> Giới thiệu
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="material-icons">view_day</i> Khu vực
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-with-icons scroll">
                            @foreach ($listArea as $area)
                                <li>
                                    <a href="/pitch_page/{{ $area->id }}">
                                        <i class="material-icons">location_on</i> {{ $area->area_name }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    <li>
                        <a href="/contact-us">
                            <i class="material-icons">perm_phone_msg</i> Liên hệ
                        </a>
                    </li>
                    @if ($customer == '')
                        <li class="button-container">
                            <a href="/login" class="btn btn-rose btn-round">
                                Đăng nhập
                            </a>
                        </li>
                        <li class="button-container">
                            <a href="/signup" class="btn btn-rose btn-round">
                                Đăng ký
                            </a>
                        </li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="material-icons">account_circle</i> Xin chào {{ $customer->name }}
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu dropdown-with-icons">
                                <li>
                                    <a href="/profile_customer">
                                        <i class="material-icons">account_circle</i> Thông tin cá nhân
                                    </a>
                                </li>
                                <li>
                                    <a href="/change_password">
                                        <i class="material-icons">vpn_key</i> Đổi mật khẩu
                                    </a>
                                </li>
                                <li>
                                    <a href="/bill_page">
                                        <i class="material-icons">assignment</i> Hoá đơn
                                    </a>
                                </li>
                                <li>
                                    <a href="/log_out">
                                        <i class="material-icons">logout</i> Đăng xuất
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    <div class="page-header header-small" data-parallax="true"
        style="background-image: url('{{ asset('images/1632374182.jpg') }}')">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="brand">
                        <h1 class="title" style="color: black">Trang đặt sân!</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- content --}}
    <div class="main main-raised">
        @yield('content')
    </div>
    @include('layout.footer_customer')
</body>

<script src="{{ asset('assets') }}/js/jquery.min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/js/material.min.js"></script>

<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="{{ asset('assets') }}/js/moment.min.js"></script>

<!--	Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="{{ asset('assets') }}/js/nouislider.min.js" type="text/javascript"></script>

<!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
<script src="{{ asset('assets') }}/js/bootstrap-datetimepicker.js" type="text/javascript"></script>

<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="{{ asset('assets') }}/js/bootstrap-selectpicker.js" type="text/javascript"></script>

<!--	Plugin for Tags, full documentation here: http://xoxco.com/projects/code/tagsinput/  -->
<script src="{{ asset('assets') }}/js/bootstrap-tagsinput.js"></script>

<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="{{ asset('assets') }}/js/jasny-bootstrap.min.js"></script>

<!-- Plugin For Google Maps -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>



<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
<script src="{{ asset('assets') }}/js/material-kit.js?v=1.2.1" type="text/javascript"></script>

<!-- Fixed Sidebar Nav - JS For Demo Purpose, Don't Include it in your project -->
<script src="{{ asset('assets') }}/assets-for-demo/modernizr.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/assets-for-demo/vertical-nav.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {
        var slider = document.getElementById('sliderRegular');

        noUiSlider.create(slider, {
            start: 40,
            connect: [true, false],
            range: {
                min: 0,
                max: 100
            }
        });

        var slider2 = document.getElementById('sliderDouble');

        noUiSlider.create(slider2, {
            start: [20, 60],
            connect: true,
            range: {
                min: 0,
                max: 100
            }
        });



        materialKit.initFormExtendedDatetimepickers();

    });
</script>

</html>
