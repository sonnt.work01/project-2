{{-- navbar --}}
<nav class="navbar navbar-primary navbar-fixed-top navbar-color-on" id="sectionsNav">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">BÓNG ĐÁ BÁCH KHOA SPORT</a>
        </div>

        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="/about-us.html">
                        <i class="material-icons">apps</i> Giới thiệu
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="material-icons">view_day</i> Khu vực
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu dropdown-with-icons scroll">
                        @foreach ($listArea as $area)
                            <li>
                                <a href="/pitch_page/{{ $area->id }}">
                                    <i class="material-icons">location_on</i> {{ $area->area_name }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                <li>
                    <a href="/contact-us">
                        <i class="material-icons">perm_phone_msg</i> Liên hệ
                    </a>
                </li>
                @if ($customer == '')
                    <li class="button-container">
                        <a href="/login" class="btn btn-rose btn-round">
                            Đăng nhập
                        </a>
                    </li>
                    <li class="button-container">
                        <a href="/signup" class="btn btn-rose btn-round">
                            Đăng ký
                        </a>
                    </li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="material-icons">account_circle</i> Xin chào {{ $customer->name }}
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-with-icons">
                            <li>
                                <a href="/profile_customer">
                                    <i class="material-icons">account_circle</i> Thông tin cá nhân
                                </a>
                            </li>
                            <li>
                                <a href="/change_password">
                                    <i class="material-icons">vpn_key</i> Đổi mật khẩu
                                </a>
                            </li>
                            <li>
                                <a href="/bill_page">
                                    <i class="material-icons">assignment</i> Hoá đơn
                                </a>
                            </li>
                            <li>
                                <a href="/log_out">
                                    <i class="material-icons">logout</i> Đăng xuất
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
