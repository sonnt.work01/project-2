{{-- footer --}}
<footer class="footer footer-black footer-big">
    <div class="container">
        <div class="content">
            <div class="row">
                <div class="col-md-4">
                    @foreach ($introduce as $data)
                        <h5>{{ $data->company }}</h5>
                        <p>{{ $data->content }}</p>
                    @endforeach
                </div>

                <div class="col-md-4">
                    <h5>Social Feed</h5>
                    <div class="social-feed">
                        <div class="feed-line">
                            <i class="fa fa-facebook-square"></i>
                            <p>FACEBOOK</p>
                        </div>
                        <div class="feed-line">
                            <i class="fa fa-instagram"></i>
                            <p>INSTAGRAM</p>
                        </div>
                        <div class="feed-line">
                            <i class="fa fa-skype"></i>
                            <p>SKYPE</p>
                        </div>

                    </div>
                </div>

                <div class="col-md-4">
                    <h5>Instagram Feed</h5>
                    <div class="gallery-feed">
                        <img src="{{ asset('assets') }}/img/faces/card-profile6-square.jpg"
                            class="img img-raised img-rounded" alt="" />
                        <img src="{{ asset('assets') }}/img/faces/christian.jpg" class="img img-raised img-rounded"
                            alt="" />
                        <img src="{{ asset('assets') }}/img/faces/card-profile4-square.jpg"
                            class="img img-raised img-rounded" alt="" />
                        <img src="{{ asset('assets') }}/img/faces/card-profile1-square.jpg"
                            class="img img-raised img-rounded" alt="" />

                        <img src="{{ asset('assets') }}/img/faces/marc.jpg" class="img img-raised img-rounded"
                            alt="" />
                        <img src="{{ asset('assets') }}/img/faces/kendall.jpg" class="img img-raised img-rounded"
                            alt="" />
                        <img src="{{ asset('assets') }}/img/faces/card-profile5-square.jpg"
                            class="img img-raised img-rounded" alt="" />
                        <img src="{{ asset('assets') }}/img/faces/card-profile2-square.jpg"
                            class="img img-raised img-rounded" alt="" />
                    </div>

                </div>
            </div>
        </div>
        <hr />
        <div class="copyright pull-right">
            Created by Sơn - Linh
        </div>
    </div>
</footer>
