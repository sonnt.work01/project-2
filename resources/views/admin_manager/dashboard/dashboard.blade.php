@extends('layout.admin')

@section('content')
    @if ($message = Session::get('error'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-danger">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <form action="" method="get">
        @if ($admin->role == 1)
            <div class="col-md-3 col-sm-2">
                <select name="area_id" class="selectpicker" data-style="btn btn-primary btn-round">
                    <option value="" @if ($area_id == null) ? selected @endif>Tất cả</option>
                    @foreach ($listArea as $data)
                        <option value="{{ $data->id }}" @if ($data->id == $area_id) ? selected @endif>{{ $data->area_name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-9 col-sm-10">
                <input type="month" name="search" value="{{ $search }}" style="padding: 5px">
                <button class="btn btn-info">Tìm kiếm</button>
            </div>
        @else
            <div class="col-md-12 col-sm-12">
                <input type="month" name="search" value="{{ $search }}" style="padding: 5px">
                <button class="btn btn-info">Tìm kiếm</button>
            </div>
        @endif
    </form>
    <div>
        <h1>Doanh thu</h1>
    </div>
    @if ($area_id == null && $admin->role == 1)
        <div id="khu_vuc_col" style="width:100%; height:400px;"></div>
        <div id="khu_vuc_pie"></div>
    @endif
    @if ($area_id != '')
        <div id="san_col" style="width:100%; height:400px;"></div>
        <div id="san_pie"></div>
        <h2>Top sân được đặt nhiều nhất trong tháng</h2>

        <div class="container-fluid">
            <div class="row">
                @foreach ($top_pitch as $item)
                    <div class="col-sm-4">
                        <div style="background-color: rgb(255, 217, 0)" class="card bg-warning">
                            <div class="card-content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <span style="font-size: 40px" class="material-icons">
                                            insights
                                        </span>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="pitch_name">
                                            <span
                                                style="color: rgb(13, 153, 0); font-size:25px"><b>{{ $item->pitch_name }}</b></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="numbers">
                                    <span
                                        style="color: rgb(252, 0, 0); font-size:25px"><b>{{ $item->so_hoa_don . ' hoá đơn' }}</b></span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <h2>Top khung giờ đặt nhiều nhất trong tháng</h2>

        <div class="container-fluid">
            <div class="row">
                @foreach ($top_time as $item)
                    <div class="col-sm-4">
                        <div style="background-color: rgb(245, 118, 0)" class="card">
                            <div class="card-content">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <span style="font-size: 40px; color:white" class="material-icons">
                                            pending_actions
                                        </span>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="pitch_name">
                                            <span
                                                style="color: rgb(255, 255, 255); font-size:21px"><b>{{ $item->time_start . '-' . $item->time_end }}</b></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="numbers">
                                    <span
                                        style="color: rgb(252, 252, 252); font-size:25px"><b>{{ $item->so_hoa_don . ' hóa đơn' }}</b></span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/series-label.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script type="text/javascript">
        @php
        $js_arr_area = json_encode($arr_area);
        $js_doanh_thu_khu_vuc = json_encode($arr_doanh_thu_khu_vuc);
        $js_arr_pitch = json_encode($arr_pitch);
        $js_doanh_thu_san = json_encode($arr_doanh_thu_san);
        $area_id = json_encode($area_id);
        @endphp
        const arr_phan_tram_doanh_thu_khu_vuc_theo_thang = [];
        const arr_doanh_thu_khu_vuc_theo_thang = [];
        const arr_phan_tram_doanh_thu_san_theo_thang = [];
        const arr_doanh_thu_san_theo_thang = [];
        var arr_area = {!! $js_arr_area !!};
        var arr_doanh_thu_khu_vuc = {!! $js_doanh_thu_khu_vuc !!};
        let tong_doanh_thu_khu_vuc_theo_thang = 0;
        var arr_pitch = {!! $js_arr_pitch !!};
        var arr_doanh_thu_san = {!! $js_doanh_thu_san !!};
        let tong_doanh_thu_san_theo_thang = 0;
        var $area_id = {!! $area_id !!};

        for (var i = 0; i < arr_area.length; i++) {
            tong_doanh_thu_khu_vuc_theo_thang += arr_doanh_thu_khu_vuc[i];
        }
        for (var i = 0; i < arr_area.length; i++) {
            arr_phan_tram_doanh_thu_khu_vuc_theo_thang.push({
                name: arr_area[i],
                y: (arr_doanh_thu_khu_vuc[i] / tong_doanh_thu_khu_vuc_theo_thang) * 100
            });
        }
        for (var i = 0; i < arr_area.length; i++) {
            arr_doanh_thu_khu_vuc_theo_thang.push([arr_area[i], arr_doanh_thu_khu_vuc[i]]);
        }


        for (var i = 0; i < arr_pitch.length; i++) {
            tong_doanh_thu_san_theo_thang += arr_doanh_thu_san[i];
        }
        for (var i = 0; i < arr_pitch.length; i++) {
            arr_phan_tram_doanh_thu_san_theo_thang.push({
                name: arr_pitch[i],
                y: (arr_doanh_thu_san[i] / tong_doanh_thu_san_theo_thang) * 100
            });
        }
        for (var i = 0; i < arr_pitch.length; i++) {
            arr_doanh_thu_san_theo_thang.push([arr_pitch[i], arr_doanh_thu_san[i]]);
        }
        if ($area_id == null) {
            Highcharts.chart('khu_vuc_col', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Doanh thu các khu vực'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Doanh thu'
                    }
                },
                legend: {
                    enabled: false
                },
                // tooltip: {
                //     pointFormat: 'Doanh thu: <b>{point.y:.1f} </b>'
                // },
                series: [{
                    name: 'Doanh thu',
                    data: arr_doanh_thu_khu_vuc_theo_thang,
                }]
            });

            Highcharts.chart('khu_vuc_pie', {
                chart: {
                    type: 'pie'
                },
                title: {
                    text: 'Phần trăm doanh thu của các khu vực'
                },
                accessibility: {
                    announceNewData: {
                        enabled: true
                    },
                    point: {
                        valueSuffix: '%'
                    }
                },

                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}: {point.y:.1f}%'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },

                series: [{
                    name: "Khu vực",
                    colorByPoint: true,
                    data: arr_phan_tram_doanh_thu_khu_vuc_theo_thang,
                }],
            });
        }
        Highcharts.chart('san_col', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Doanh thu các sân'
            },
            xAxis: {
                type: 'category',
                labels: {
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Doanh thu'
                }
            },
            legend: {
                enabled: false
            },
            // tooltip: {
            //     pointFormat: 'Doanh thu: <b>{point.y:.1f} </b>'
            // },
            series: [{
                name: 'Doanh thu',
                data: arr_doanh_thu_san_theo_thang,
                // dataLabels: {
                //     enabled: true,
                //     rotation: -90,
                //     color: '#FFFFFF',
                //     align: 'right',
                //     format: '{point.y:.1f}', // one decimal
                //     y: 10, // 10 pixels down from the top
                //     style: {
                //         fontSize: '13px',
                //         fontFamily: 'Verdana, sans-serif'
                //     }
                // }
            }]
        });

        Highcharts.chart('san_pie', {
            chart: {
                type: 'pie'
            },
            title: {
                text: 'Phần trăm doanh thu của các sân'
            },
            accessibility: {
                announceNewData: {
                    enabled: true
                },
                point: {
                    valueSuffix: '%'
                }
            },

            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}: {point.y:.1f}%'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },

            series: [{
                name: "Khu vực",
                colorByPoint: true,
                data: arr_phan_tram_doanh_thu_san_theo_thang,
            }],
        });
    </script>
@endsection
