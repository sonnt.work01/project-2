@extends('layout.admin')

@section('content')
@if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @elseif ($message = Session::get('error'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-danger">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <h1>Sửa mật khẩu</h1>
    {{-- validate --}}
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li><span style="font-size: 19px">{{ $error }}</span></li>
            @endforeach
        </ul>
    </div>
@endif
    <form action="{{ route('change_password_admin.update', $admin->id) }}" method="post">
        @method('PUT')
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>
                        Mật khẩu hiện tại
                    </th>
                    <td><input type="password" name="old_password" class="form-control" required></td>
                </tr>
                <tr>
                    <th>
                        Mật khẩu mới
                    </th>
                    <td><input type="password" name="new_password" class="form-control" required></td>
                </tr>
                <tr>
                    <th>
                        Xác nhập lại mật khẩu
                    </th>
                    <td><input type="password" name="repeat_new_password" class="form-control" required></td>
                </tr>

                <tr>
                    <td colspan="2">
                        <button class="btn btn-info">Cập nhật</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
