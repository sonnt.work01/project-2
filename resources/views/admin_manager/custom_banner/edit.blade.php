@extends('layout.admin')

@section('content')
    @if ($message = Session::get('error'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-danger">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <a href="/custom_banner" class="btn btn-primary">Quay lại</a>
    <h1>Sửa thông tin khu vực</h1>
    {{-- validate --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('custom_banner.update', $banner->id) }}" method="post" enctype="multipart/form-data">
        @method('PATCH')
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>
                        Chọn ảnh banner
                    </th>
                    <td>
                        <img src="{{ asset('images/' . $banner->image_path) }}" style="width: 350px; height:350px">
                        <input type="file" name="image">
                        <input type="hidden" name="hidden_image" value="{{ $banner->image_path }}">
                    </td>
                </tr>
                <tr>
                    <th>
                        Vị trí
                    </th>
                    <td>
                        @if ($banner->location == 1)
                            <span style="color: black; font-size:20px">{{ 'Banner trên đầu trang chủ' }}</span>
                        @elseif($banner->location == 2)
                            <span style="color: black; font-size:20px">{{ 'Banner dưới trang chủ' }}</span>
                        @elseif($banner->location == 3)
                            <span style="color: black; font-size:20px">{{ 'Banner trang giới thiệu' }}</span>
                        @elseif($banner->location == 4)
                            <span style="color: black; font-size:20px">{{ 'Banner trang đặt sân' }}</span>
                        @elseif($banner->location == 5)
                            <span style="color: black; font-size:20px">{{ 'Banner trang blog' }}</span>
                        @elseif($banner->location == 6)
                            <span style="color: black; font-size:20px">{{ 'Banner trang liên hệ' }}</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button class="btn btn-info">Cập nhật</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
