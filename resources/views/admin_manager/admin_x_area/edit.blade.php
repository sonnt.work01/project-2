@extends('layout.admin')

@section('content')
    @if ($message = Session::get('error'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-danger">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <a href="/admin_x_area" class="btn btn-primary">Quay lại</a>
    <h1>Sửa Quản lý khu vực</h1>
    {{-- validate --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('admin_x_area.update', $adminxarea->admin_id) }}" method="post">
        @method('PUT')
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <td>
                        <div class="col-md-5">
                            <select name="admin_id" class="selectpicker" data-style="btn btn-primary btn-round">
                                @if ($listAdmin->count() == 0)
                                    <option disabled>Không còn nhân viên phù hợp</option>
                                @else
                                    @foreach ($listAdmin as $data)
                                        <option value="{{ $data->id }}" @if ($adminxarea->admin_id == $data->id) selected @endif>
                                            {{ $data->name }}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="col-md-5">
                            <select name="area_id" class="selectpicker" data-style="btn btn-primary btn-round">
                                @if ($listArea->count() == 0)
                                    <option disabled>Không còn khu vực phù hợp</option>
                                @else
                                    @foreach ($listArea as $data)
                                        <option value="{{ $data->id }}" @if ($adminxarea->area_id == $data->id) selected @endif>
                                            {{ $data->area_name }}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Trạng thái </b>
                        <input type="radio" name="del_flag" id="1" value="1" @if ($adminxarea->del_flag == 1) checked @endif><label for="1"
                            style="color: black"><b>Hoạt động</b></label>
                        <input type="radio" name="del_flag" id="0" value="0" @if ($adminxarea->del_flag == 0) checked @endif><label for="0"
                            style="color: black"><b>Ngừng hoạt động</b></label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button class="btn btn-info">Cập nhật</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
