@extends('layout.admin')

@section('content')
<a href="/blog" class="btn btn-primary">Quay lại</a>
    <h1>Sửa thông tin Blog</h1>
    {{-- validate --}}
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li><span style="font-size: 19px">{{ $error }}</span></li>
            @endforeach
        </ul>
    </div>
    @endif
    @if ($message = Session::get('error'))
    <div class="section cd-section section-notifications" id="notifications">
        <div class="alert alert-danger">
            <div>
                <div class="alert-icon">
                    <i class="material-icons">check</i>
                </div>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="material-icons">clear</i></span>
                </button>
                <h3>{{ $message }}</h3>
            </div>
        </div>
    </div>
    @endif
    <form action="{{ route('blog.update', $blog->id) }}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>
                        Tiêu đề
                    </th>
                    <td width="80%"><input  type="text" name="title" value="{{ $blog->title }}" class="form-control" required></td>
                </tr>
                <tr>
                    <th>
                        Người đăng
                    </th>
                    <td width="80%">
                        <select name="admin_id" class="form-control">
                            <option value="{{ $admin->id }}">
                                {{ $admin->name }}
                            </option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>
                        Ảnh đại diện
                    </th>
                    <td>
                        <img src="{{ asset('images/' . $blog->image_path) }}" style="width: 350px; height:350px">
                        <input type="file" name="image">
                        <input type="hidden" name="hidden_image" value="{{ $blog->image_path }}">
                    </td>
                </tr>
                <tr>
                    <th>
                        Ngày đăng
                    </th>
                    <td width="80%">
                        <input type="date" name="date_submitted" value="{{ $blog->date_submitted }}" min="{{ $blog->date_submitted }}" class="form-control" required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Nội dung
                    </th>
                    <td width="80%">
                        <textarea name="content" class="form-control" value="{{ $blog->content }}" rows="5" required>{{ $blog->content }}</textarea>
                    </td>
                </tr>
                <tr>
                    <th>
                        Trạng thái
                    </th>
                    <td width="80%">
                        <input type="radio" name="del_flag" value="1" id="1" @if ( $blog->del_flag == 1 ) checked @endif ><label for="1" style="color: black"><b>Hiện</b></label>
                        <input type="radio" name="del_flag" value="0" id="0" @if ( $blog->del_flag == 0 ) checked @endif ><label for="0" style="color: black"><b>Ẩn</b></label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button class="btn btn-info">Cập nhật</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
