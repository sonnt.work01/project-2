@extends('layout.admin')

@section('content')
    @if ($message = Session::get('error'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-danger">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <a href="/admin" class="btn btn-primary">Quay lại</a>
    <h1>Sửa thông tin nhân viên</h1>
    {{-- validate --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('admin.update', $admin_edit->id) }}" method="post">
        @method('PUT')
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>
                        Họ Tên
                    </th>
                    <td><input type="text" name="name" value="{{ $admin_edit->name }}" class="form-control" required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Email
                    </th>
                    <td>
                        <input type="text" name="email" value="{{ $admin_edit->email }}" class="form-control" required readonly>
                    </td>
                </tr>
                <tr>
                    <th>
                        Ngày sinh
                    </th>
                    <td><input type="date" name="date_birth" value="{{ $admin_edit->date_birth }}" class="form-control"
                            required></td>
                </tr>
                <tr>
                    <th>
                        Giới tính
                    </th>
                    <td>
                        <input type="radio" name="gender" value="1" id="1" @if ($admin_edit->gender == 1) checked @endif><label
                            for="1">Nam</label>
                        <input type="radio" name="gender" value="0" id="0" @if ($admin_edit->gender == 0) checked @endif><label
                            for="0">Nữ</label>
                    </td>
                </tr>
                <tr>
                    <th>
                        Số điện thoại
                    </th>
                    <td><input type="text" name="phone" value="{{ $admin_edit->phone }}" class="form-control" required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Địa chỉ
                    </th>
                    <td><input type="text" name="address" value="{{ $admin_edit->address }}" class="form-control"
                            required></td>
                </tr>
                <tr>
                    <th>
                        Trạng thái hoạt động
                    </th>
                    <td>
                        <input type="radio" name="del_flag" id="2" value="1" @if ($admin_edit->del_flag == 1) checked @endif><label for="2">Hoạt
                            động</label>
                        <input type="radio" name="del_flag" id="3" value="0" @if ($admin_edit->del_flag == 0) checked @endif><label for="3">Nghỉ
                            việc</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button class="btn btn-info">Cập nhật</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
