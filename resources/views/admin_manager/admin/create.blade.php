@extends('layout.admin')

@section('content')
    <a href="/admin" class="btn btn-primary">Quay lại</a>
    <h1>Thêm nhân viên</h1>
    {{-- validate --}}
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('admin.store') }}" method="post">
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>
                        Họ Tên
                    </th>
                    <td>
                        <input type="text" name="name" class="form-control" required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Email
                    </th>
                    <td>
                        <input type="text" name="email" class="form-control" required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Password
                    </th>
                    <td>
                        <input type="password" name="password" class="form-control" required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Giới tính
                    </th>
                    <td>
                        <input type="radio" name="gender" value="1" checked> Nam
                        <input type="radio" name="gender" value="0"> Nữ
                    </td>
                </tr>
                <tr>
                    <th>
                        Số điện thoại
                    </th>
                    <th>
                        <input type="text" name="phone" class="form-control" required>
                        </td>
                </tr>
                <tr>
                    <th>
                        Ngày sinh
                    </th>
                    <td>
                        <input type="date" name="date_birth" class="form-control" required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Địa chỉ
                    </th>
                    <td>
                        <input type="text" name="address" class="form-control" required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Vai trò
                    </th>
                    <td>
                        <input type="text" name="role" value="Nhân viên" class="form-control" readonly>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button class="btn btn-info">Thêm nhân viên</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
