@extends('layout.admin')

@section('content')
    <h1>Hoá đơn</h1>
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-danger">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <form action="" method="get">
        @if($admin->role == 1)
        <div class="col-md-4 col-sm-4">
            <select name="area_id" class="selectpicker" data-style="btn btn-primary btn-round">
                @foreach ($area as $data)
                    <option value="{{ $data->id }}" @if ($data->id == $area_id) ? selected @endif>{{ $data->area_name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-5 col-sm-5">
            <select name="bill_active" class="selectpicker" data-style="btn btn-primary btn-round">
                <option value="1" @if ($bill_active == 1) ? selected @endif>Chưa xử lý</option>
                <option value="2" @if ($bill_active == 2) ? selected @endif>Đã đặt</option>
                <option value="3" @if ($bill_active == 3) ? selected @endif>Đã huỷ</option>
                <option value="4" @if ($bill_active == 4) ? selected @endif>Đã thanh toán</option>
            </select>
        </div>
        <div class="col-md-3 col-sm-3">
            <input type="date" name="search" value="{{ $search }}" style="padding: 5px">
            <button class="btn btn-info">Tìm kiếm</button>
        </div>
        @else
        <div class="col-md-6 col-sm-6">
            <select name="bill_active" class="selectpicker" data-style="btn btn-primary btn-round">
                <option value="1" @if ($bill_active == 1) ? selected @endif>Chưa xử lý</option>
                <option value="2" @if ($bill_active == 2) ? selected @endif>Đã đặt</option>
                <option value="3" @if ($bill_active == 3) ? selected @endif>Đã huỷ</option>
                <option value="4" @if ($bill_active == 4) ? selected @endif>Đã thanh toán</option>
            </select>
        </div>
        <div class="col-md-6 col-sm-6">
            <input type="date" name="search" value="{{ $search }}" style="padding: 5px">
            <button class="btn btn-info">Tìm kiếm</button>
        </div>
        @endif
    </form>
    @if ($listBill->count() == 0)
        {{ 'Không có bản ghi' }}
    @else
        @if ($bill_active == 1)
            <div class="material-datatables">
                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0"
                    width="100%" style="width:100%">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>
                                Tên khách hàng
                            </th>
                            <th>
                                Tên khu vực
                            </th>
                            <th>
                                Tên sân
                            </th>
                            <th>
                                Ngày
                            </th>
                            <th>
                                Thời gian
                            </th>
                            <th>
                                Giá sân
                            </th>
                            <th>
                                Đặt cọc
                            </th>
                            <th>
                                Tiền còn lại
                            </th>
                            <th>
                                Trạng thái
                            </th>
                            <th>
                                Hành động
                            </th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>STT</th>
                            <th>
                                Tên khách hàng
                            </th>
                            <th>
                                Tên khu vực
                            </th>
                            <th>
                                Tên sân
                            </th>
                            <th>
                                Ngày
                            </th>
                            <th>
                                Thời gian
                            </th>
                            <th>
                                Giá sân
                            </th>
                            <th>
                                Đặt cọc
                            </th>
                            <th>
                                Tiền còn lại
                            </th>
                            <th>
                                Trạng thái
                            </th>
                            <th>
                                Hành động
                            </th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php $i = 0; ?>
                        @foreach ($listBill as $listBill)
                            <?php $i++; ?>
                            <tr>
                                <td>{{ $i }}</td>
                                <th>
                                    <b>{{ $listBill->name }}</b>
                                </th>
                                <td>
                                    {{ $listBill->area_name }}
                                </td>
                                <td>
                                    {{ $listBill->pitch_name }}
                                </td>
                                <td>
                                    {{ $listBill->day }}
                                </td>
                                <td>
                                    {{ $listBill->time_start . '-' . $listBill->time_end }}
                                </td>
                                <td>
                                    {{ $listBill->price }}
                                </td>
                                <td>
                                    {{ $listBill->deposit }}
                                </td>
                                <td>
                                    {{ $listBill->missing }}
                                </td>
                                <td>
                                    {{ $listBill->ActiveName }}
                                </td>
                                <td>
                                    <form action="{{ route('bill.update', $listBill->id) }}" method="post">
                                        @csrf
                                        @method('PATCH')
                                        <button class="btn btn-success">Duyệt</button>
                                    </form>
                                    <form action="{{ route('bill.destroy', $listBill->id) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button onclick="return confirm('Bạn có chắc chắn hủy đơn này?')"
                                            class="btn btn-danger">Huỷ đơn</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @elseif ($bill_active == 2)
            <div class="material-datatables">
                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0"
                    width="100%" style="width:100%">
                    <thead>
                        <th>STT</th>
                        <th>
                            Tên khách hàng
                        </th>
                        <th>
                            Tên khu vực
                        </th>
                        <th>
                            Tên sân
                        </th>
                        <th>
                            Ngày
                        </th>
                        <th>
                            Thời gian
                        </th>
                        <th>
                            Giá sân
                        </th>
                        <th>
                            Đặt cọc
                        </th>
                        <th>
                            Tiền còn lại
                        </th>
                        <th>
                            Trạng thái
                        </th>
                        <th>
                            Thanh toán
                        </th>
                        </tr>
                    </thead>
                    <tfoot>
                        <th>STT</th>
                        <th>
                            Tên khách hàng
                        </th>
                        <th>
                            Tên khu vực
                        </th>
                        <th>
                            Tên sân
                        </th>
                        <th>
                            Ngày
                        </th>
                        <th>
                            Thời gian
                        </th>
                        <th>
                            Giá sân
                        </th>
                        <th>
                            Đặt cọc
                        </th>
                        <th>
                            Tiền còn lại
                        </th>
                        <th>
                            Trạng thái
                        </th>
                        <th>
                            Thanh toán
                        </th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php $i = 0; ?>
                        @foreach ($listBill as $listBill)
                            <?php $i++; ?>
                            <tr>
                                <th>{{ $i }}</th>
                                <td>
                                    <b>{{ $listBill->name }}</b>
                                </td>
                                <td>
                                    {{ $listBill->area_name }}
                                </td>
                                <td>
                                    {{ $listBill->pitch_name }}
                                </td>
                                <td>
                                    {{ $listBill->day }}
                                </td>
                                <td>
                                    {{ $listBill->time_start . '-' . $listBill->time_end }}
                                </td>
                                <td>
                                    {{ $listBill->price }}
                                </td>
                                <td>
                                    {{ $listBill->deposit }}
                                </td>
                                <td>
                                    {{ $listBill->missing }}
                                </td>
                                <td>
                                    <span style="background-color: green; color:white;padding:6px; border-radius:5px">
                                        {{ $listBill->ActiveName }}
                                    </span>
                                </td>
                                <td>
                                    <form action="{{ route('bill.update', $listBill->id) }}" method="post">
                                        @csrf
                                        @method('PATCH')
                                        <button class="btn btn-success">Thanh toán</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @elseif($bill_active == 3)
            <div class="material-datatables">
                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0"
                    width="100%" style="width:100%">
                    <thead>
                        <th>STT</th>
                        <th>
                            Tên khách hàng
                        </th>
                        <th>
                            Tên khu vực
                        </th>
                        <th>
                            Tên sân
                        </th>
                        <th>
                            Ngày
                        </th>
                        <th>
                            Thời gian
                        </th>
                        <th>
                            Giá sân
                        </th>
                        <th>
                            Đặt cọc
                        </th>
                        <th>
                            Tiền còn lại
                        </th>
                        <th>
                            Trạng thái
                        </th>
                        </tr>
                    </thead>
                    <tfoot>
                        <th>STT</th>
                        <th>
                            Tên khách hàng
                        </th>
                        <th>
                            Tên khu vực
                        </th>
                        <th>
                            Tên sân
                        </th>
                        <th>
                            Ngày
                        </th>
                        <th>
                            Thời gian
                        </th>
                        <th>
                            Giá sân
                        </th>
                        <th>
                            Đặt cọc
                        </th>
                        <th>
                            Tiền còn lại
                        </th>
                        <th>
                            Trạng thái
                        </th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php $i = 0; ?>
                        @foreach ($listBill as $listBill)
                            <?php $i++; ?>
                            <tr>
                                <th>{{ $i }}</th>
                                <td>
                                    <b>{{ $listBill->name }}</b>
                                </td>
                                <td>
                                    {{ $listBill->area_name }}
                                </td>
                                <td>
                                    {{ $listBill->pitch_name }}
                                </td>
                                <td>
                                    {{ $listBill->day }}
                                </td>
                                <td>
                                    {{ $listBill->time_start . '-' . $listBill->time_end }}
                                </td>
                                <td>
                                    {{ $listBill->price }}
                                </td>
                                <td>
                                    {{ $listBill->deposit }}
                                </td>
                                <td>
                                    {{ $listBill->missing }}
                                </td>
                                <td>

                                    <span style="background-color: red; color:white;padding:6px; border-radius:5px">
                                        {{ $listBill->ActiveName }}
                                    </span>

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="material-datatables">
                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0"
                    width="100%" style="width:100%">
                    <thead>
                        <th>STT</th>
                        <th>
                            Tên khách hàng
                        </th>
                        <th>
                            Tên khu vực
                        </th>
                        <th>
                            Tên sân
                        </th>
                        <th>
                            Ngày
                        </th>
                        <th>
                            Thời gian
                        </th>
                        <th>
                            Giá sân
                        </th>
                        <th>
                            Trạng thái
                        </th>
                        </tr>
                    </thead>
                    <tfoot>
                        <th>STT</th>
                        <th>
                            Tên khách hàng
                        </th>
                        <th>
                            Tên khu vực
                        </th>
                        <th>
                            Tên sân
                        </th>
                        <th>
                            Ngày
                        </th>
                        <th>
                            Thời gian
                        </th>
                        <th>
                            Giá sân
                        </th>
                        <th>
                            Trạng thái
                        </th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php $i = 0; ?>
                        @foreach ($listBill as $listBill)
                            <?php $i++; ?>
                            <tr>
                                <th>{{ $i }}</th>
                                <td>
                                    <b>{{ $listBill->name }}</b>
                                </td>
                                <td>
                                    {{ $listBill->area_name }}
                                </td>
                                <td>
                                    {{ $listBill->pitch_name }}
                                </td>
                                <td>
                                    {{ $listBill->day }}
                                </td>
                                <td>
                                    {{ $listBill->time_start . '-' . $listBill->time_end }}
                                </td>
                                <td>
                                    {{ $listBill->price }}
                                </td>
                                <td>

                                    <span style="background-color: green; color:white;padding:6px; border-radius:5px">
                                        {{ $listBill->ActiveName }}
                                    </span>

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    @endif
@endsection
