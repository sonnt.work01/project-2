@extends('layout.admin')

@section('content')
    @if ($message = Session::get('error'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-danger">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <a href="/profile_admin" class="btn btn-primary">Quay lại</a>
    <h1>Sửa thông tin cá nhân</h1>
    {{-- validate --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('profile_admin.update', $profile_admin->id) }}" method="post">
        @method('PUT')
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>
                        Họ Tên
                    </th>
                    <td><input type="text" name="name" value="{{ $profile_admin->name }}" class="form-control" required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Email
                    </th>
                    <td><input type="text" name="email" value="{{ $profile_admin->email }}" class="form-control" readonly></td>
                </tr>
                <tr>
                    <th>
                        Ngày sinh
                    </th>
                    <td><input type="date" name="date_birth" value="{{ $profile_admin->date_birth }}"
                            class="form-control" required></td>
                </tr>
                <tr>
                    <th>
                        Giới tính
                    </th>
                    <td>
                        <input type="radio" name="gender" value="1" @if ($profile_admin->gender == 1) checked @endif>Nam
                        <input type="radio" name="gender" value="0" @if ($profile_admin->gender == 0) checked @endif>Nữ
                    </td>
                </tr>
                <tr>
                    <th>
                        Số điện thoại
                    </th>
                    <td><input type="text" name="phone" value="{{ $profile_admin->phone }}" class="form-control"
                            required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Địa chỉ
                    </th>
                    <td><input type="text" name="address" value="{{ $profile_admin->address }}" class="form-control"
                            required></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button class="btn btn-info">Cập nhật</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
