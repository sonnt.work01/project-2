@extends('layout.admin')

@section('content')
<div class="container-fluid">
            @if ($message = Session::get('success'))
                <div class="section cd-section section-notifications" id="notifications">
                    <div class="alert alert-success">
                        <div>
                            <div class="alert-icon">
                                <i class="material-icons">check</i>
                            </div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="material-icons">clear</i></span>
                            </button>
                            <h3>{{ $message }}</h3>
                        </div>
                    </div>
                </div>
            @endif
            <div class="material-datatables">
                <div class="text-center">
                    <h1>Thông tin cá nhân</h1>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>Họ Tên</th>
                            <th>Email</th>
                            <th>Ngày sinh</th>
                            <th>Giới tính</th>
                            <th>Số điện thoại</th>
                            <th>Địa chỉ</th>
                            <th>Hành động</th>
                        </tr>
                        <tr>
                            <td><b>{{ $profile_admin->name }}</b></td>
                            <td>{{ $profile_admin->email }}</td>
                            <td>{{ $profile_admin->date_birth }}</td>
                            <td>{{ $profile_admin->GenderName }}</td>
                            <td>{{ $profile_admin->phone }}</td>
                            <td>{{ $profile_admin->address }}</td>
                            <td><a href="{{route('profile_admin.edit',$profile_admin->id)}}" class="btn btn-success">Sửa</a></td>
                        </tr>
                    </table>
                </div>
        </div>
@endsection
