@extends('layout.admin')

@section('content')
@if ($message = Session::get('success'))
<div class="section cd-section section-notifications" id="notifications">
    <div class="alert alert-success">
        <div>
            <div class="alert-icon">
                <i class="material-icons">check</i>
            </div>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="material-icons">clear</i></span>
            </button>
            <h3>{{ $message }}</h3>
        </div>
    </div>
</div>
@endif
    <h1>Thông tin giới thiệu</h1>

    <a href="{{ route('introduce.create') }}" class="btn btn-primary"><i class="material-icons">person_add</i>Thêm thông tin</a>
    @if ($listIntroduce->count() == 0)
        {{ "Không có bản ghi" }}
    @else
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>     
                <th>STT</th>      
                <th>Tên công ty</th>
                <th>Ảnh đại diện</th>
                <th>Thông tin giới thiệu</th>
                <th>Địa chỉ công ty</th>
                <th>Ngày đăng</th>
                <th>Trạng thái</th>
                <th>Sửa thông tin</th>
            </tr>
            <?php $i = 0 ?>
            @foreach ($listIntroduce as $introduce)
                        <?php $i++ ?>
                <tr>
                    <td>{{ $i }}</td>
                    <th>{{ $introduce->company }}</th>
                    <td>
                        <img src="{{ asset('images/' . $introduce->avatar) }}" style="width:200px; height:200px">
                    </td>
                    <td>{{ $introduce->content }}</td>
                    <td>{{ $introduce->address }}</td>
                    <td>{{ $introduce->date_submitted }}</td>
                    <td>
                        {{ $introduce->del_flag == 1 ? 'Hiển thị' : 'Ẩn' }}
                    </td>
                    <td><a href="{{ route('introduce.edit', $introduce->id) }}" class="btn btn-success">Sửa</a></td>
                </tr>
            @endforeach
        </table>
    </div>
    @endif
{{ $listIntroduce->appends([
    'search' => $search
])->links() }}
@endsection