@extends('layout.admin')

@section('content')
    <a href="/introduce" class="btn btn-primary">Quay lại</a>
    <h1>Thêm thông tin giới thiệu</h1>
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    {{-- validate --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('introduce.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>
                        Tên công ty
                    </th>
                    <td>
                        <input type="text" name="company" class="form-control" required>
                    </td>
                </tr>
                <tr>
                    <th>
                        Ảnh đại diện
                    </th>
                    <td>
                        <input type="file" name="avatar">
                    </td>
                </tr>
                <tr>
                    <th>
                        Thông tin giới thiệu
                    </th>
                    <td>
                        <textarea name="content" class="form-control" rows="5"></textarea>
                    </td>
                </tr>
                <tr>
                    <th>
                        Địa chỉ
                    </th>
                    <td>
                        <input type="text" name="address" class="form-control" required>

                    </td>
                </tr>
                <tr>
                    <th>
                        Ngày đăng
                    </th>
                    <th>
                        <input type="date" name="date_submitted" required>
                        </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button class="btn btn-info">Thêm thông tin</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
