@extends('layout.admin')

@section('content')
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <h1>Danh sách các sân</h1>
    <form action="" method="get">
        @if($admin->role == 1)
        <div class="col-md-4 col-sm-4">
            <select name="area_id" class="selectpicker" data-style="btn btn-primary btn-round">
                @foreach ($listArea as $data)
                    <option value="{{ $data->id }}" @if ($data->id == $area_id) ? selected @endif>{{ $data->area_name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-4 col-sm-4">
            <select name="def_flag" class="selectpicker" data-style="btn btn-primary btn-round">
                <option value="1" @if ($del_flag == 1) selected @endif>Hoạt động</option>
                <option value="0" @if ($del_flag == 0) selected @endif>Không hoạt động</option>
            </select>
        </div>
        <div class="col-md-4 col-sm-4">
            <input type="date" name="search" value="{{ $search }}" style="padding: 5px">
            <button class="btn btn-info">Chọn sân</button>
            <a href="{{ route('pitch.create') }}" class="btn btn-primary">
                <i class="material-icons">add_circle</i> Thêm sân
            </a>
        </div>
        @else
        <div class="col-md-4 col-sm-4">
            <select name="def_flag" class="selectpicker" data-style="btn btn-primary btn-round">
                <option value="1" @if ($del_flag == 1) selected @endif>Hoạt động</option>
                <option value="0" @if ($del_flag == 0) selected @endif>Không hoạt động</option>
            </select>
        </div>
        <div class="col-md-8 col-sm-8">
            <input type="date" name="search" value="{{ $search }}" style="padding: 5px">
            <button class="btn btn-info">Chọn sân</button>
            <a href="{{ route('pitch.create') }}" class="btn btn-primary">
                <i class="material-icons">add_circle</i> Thêm sân
            </a>
        </div>
        @endif
    </form>
    <h1>Trạng thái các sân</h1>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th>
                    Sân
                </th>
                @foreach ($listTime as $time)
                    <th>{{ $time->time_start . '-' . $time->time_end }}</th>
                @endforeach
            </tr>
            @foreach ($listPitch as $pitch)
                <tr>
                    @if ($pitch->location == 1)
                        <th>
                            {{ $pitch->pitch_name . ' (G)' }}
                        </th>
                    @else
                        <th>
                            {{ $pitch->pitch_name . ' (Đ)' }}
                        </th>
                    @endif

                    @foreach ($listTime as $time)
                        <th>
                            @foreach ($listBill as $bill)
                                @if ($bill->pitch_id == $pitch->id && $bill->time_id == $time->id && $bill->active == 2)
                                    <b style="color: green">Đã đặt</b>
                                @elseif($bill->pitch_id == $pitch->id && $bill->time_id == $time->id &&
                                    $bill->active == 1)
                                    <b style="color: orange">Đang chờ xử lý</b>
                                @endif
                            @endforeach
                        </th>
                    @endforeach
                </tr>
            @endforeach
        </table>
    </div>
    <div class="material-datatables">
        @if ($listPitch->count() == 0)
            {{ 'Không có bản ghi' }}
        @else
            <div class="table-responsive">
                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0"
                    width="100%" style="width:100%">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên sân</th>
                            <th>Khu vực</th>
                            <th>Vị trí</th>
                            <th>Loại sân</th>
                            <th>Ảnh</th>
                            <th>Giá sân theo giờ</th>
                            <th>Trạng thái</th>
                            <th>Sửa thông tin sân</th>
                            @if ($pitch->del_flag == 1)
                                <th>Đặt hộ khách</th>
                            @endif
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>STT</th>
                            <th>Tên sân</th>
                            <th>Khu vực</th>
                            <th>Vị trí</th>
                            <th>Loại sân</th>
                            <th>Ảnh</th>
                            <th>Giá sân theo giờ</th>
                            <th>Trạng thái</th>
                            <th>Sửa thông tin sân</th>
                            @if ($pitch->del_flag == 1)
                                <th>Đặt hộ khách</th>
                            @endif
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php $i = 0; ?>
                        @foreach ($listPitch as $pitch)
                            <?php $i++; ?>
                            @if ($pitch->del_flag == 1)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <th>{{ $pitch->pitch_name }}</th>
                                    <td>{{ $pitch->area_name }}</td>
                                    <td>{{ $pitch->location == 0 ? 'Sân đơn' : 'Sân ghép' }}</td>
                                    <td>{{ $pitch->pitch_type == 0 ? 'Sân 7' : 'Sân 11' }}</td>
                                    <td>
                                        <img src="{{ asset('images/' . $pitch->image_path) }}"
                                            style="width:250px; height:250px;">
                                    </td>
                                    <td>{{ $pitch->price }}</td>
                                    <td>
                                        {{ $pitch->del_flag == 1 ? 'Hoạt động' : 'Ngừng hoạt động' }}
                                    </td>
                                    <td><a href="{{ route('pitch.edit', $pitch->id) }}" class="btn btn-success">Sửa</a>
                                    </td>
                                    <td><a href="/order_admin/{{ $pitch->area_id }}/{{ $pitch->id }}/{{ $search }}"
                                            class="btn btn-success">Đặt sân</a></td>
                                </tr>
                            @elseif($pitch->del_flag == 0)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <th>{{ $pitch->pitch_name }}</th>
                                    <td>{{ $pitch->area_name }}</td>
                                    <td>{{ $pitch->location == 0 ? 'Sân đơn' : 'Sân ghép' }}</td>
                                    <td>{{ $pitch->pitch_type == 0 ? 'Sân 7' : 'Sân 11' }}</td>
                                    <td>
                                        <img src="{{ asset('images/' . $pitch->image_path) }}"
                                            style="width:250px; height:250px;">
                                    </td>
                                    <td>{{ $pitch->price }}</td>
                                    <td>
                                        {{ $pitch->del_flag == 1 ? 'Hoạt động' : 'Ngừng hoạt động' }}
                                    </td>
                                    <td><a href="{{ route('pitch.edit', $pitch->id) }}" class="btn btn-success">Sửa</a>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
@endsection
