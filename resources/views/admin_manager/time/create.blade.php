@extends('layout.admin')

@section('content')
    <a href="/time" class="btn btn-primary">Quay lại</a>
    <h1>Thêm khung thời gian</h1>
    @if ($message = Session::get('success'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-success">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-danger">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    {{-- validate --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('time.store') }}" method="post">
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <td>
                        <b>Thời gian bắt đầu</b>
                        <input type="time" name="time_start" class="form-control" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Thời gian kết thúc</b>
                        <input type="time" name="time_end" class="form-control" required>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button class="btn btn-info">Thêm</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
