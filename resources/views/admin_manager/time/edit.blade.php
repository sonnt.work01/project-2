@extends('layout.admin')

@section('content')
    @if ($message = Session::get('error'))
        <div class="section cd-section section-notifications" id="notifications">
            <div class="alert alert-danger">
                <div>
                    <div class="alert-icon">
                        <i class="material-icons">check</i>
                    </div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                    </button>
                    <h3>{{ $message }}</h3>
                </div>
            </div>
        </div>
    @endif
    <a href="/time" class="btn btn-primary">Quay lại</a>
    <h1>Sửa khung thời gian</h1>
    <div>
        <em style="color: rgb(211, 0, 0); font-size:17px">*Bạn chỉ có thể sửa trạng thái hoạt động!!!</em>
    </div>
    {{-- validate --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><span style="font-size: 19px">{{ $error }}</span></li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('time.update', $time->id) }}" method="post">
        @method('PUT')
        @csrf
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <td>
                        <b>Thời gian bắt đầu</b><input type="time" name="time_start" class="form-control"
                            value="{{ $time->time_start }}" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Thời gian Kết thúc</b> <input type="time" name="time_end" class="form-control"
                            value="{{ $time->time_end }}" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Trạng thái </b>
                        <input type="radio" name="del_flag" id="1" value="1" @if ($time->del_flag == 1) checked @endif><label for="1"
                            style="color: black"><b>Hoạt động</b></label>
                        <input type="radio" name="del_flag" id="0" value="0" @if ($time->del_flag == 0) checked @endif><label for="0"
                            style="color: black"><b>Ngừng hoạt động</b></label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button class="btn btn-info">Cập nhật</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
@endsection
