@extends('layout.admin')

@section('content')
    <div class="section">
        <div class="content">
            <a href="{{ URL::previous() }}" class="btn btn-primary">Quay lại</a>
            <div class="container-fluid">
                <div class="row">
                    <div class="col">
                        <h2 class="section-title" align="center">Xin mời bạn cung cấp đầy đủ thông tin</h2>
                    </div>
                </div>
                {{-- validate --}}
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li><span style="font-size: 19px">{{ $error }}</span></li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-9">
                        @if ($message = Session::get('success'))
                            <div class="section cd-section section-notifications" id="notifications">
                                <div class="alert alert-success">
                                    <div>
                                        <div class="alert-icon">
                                            <i class="material-icons">check</i>
                                        </div>
                                        <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                            <span aria-hidden="true"><i class="material-icons">clear</i></span>
                                        </button>
                                        <h3>{{ $message }}</h3>
                                    </div>
                                </div>
                            </div>
                        @elseif ($message = Session::get('error'))
                            <div class="section cd-section section-notifications" id="notifications">
                                <div class="alert alert-danger">
                                    <div>
                                        <div class="alert-icon">
                                            <i class="material-icons">check</i>
                                        </div>
                                        <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                            <span aria-hidden="true"><i class="material-icons">clear</i></span>
                                        </button>
                                        <h3>{{ $message }}</h3>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="card">
                            <form method="post" action="/order_admin_process" class="form-horizontal">
                                @csrf
                                <div class="card-content">
                                    <div class="row">
                                        <label class="col-sm-3 label-on-left"><b style="color: black">Email khách hàng
                                            </b><b style="color: red">*</b></label>
                                        <div class="col-sm-9">
                                            <div class="form-group label-floating is-empty">
                                                <select name="customer_id" class="form-control">
                                                    @foreach ($listCustomer as $customer)
                                                        <option value="{{ $customer->id }}">
                                                            {{ $customer->email }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-3 label-on-left"><b style="color: black">Khu vực</b></label>
                                        <div class="col-sm-9">
                                            <div class="form-group label-floating is-empty">
                                                <div class="col-lg-5 col-md-6 col-sm-3">
                                                    <select name="area_id" class="selectpicker"
                                                        data-style="btn btn-primary btn-round">
                                                        <option value="{{ $area->id }}">
                                                            {{ $area->area_name }}
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-sm-3 label-on-left"><b style="color: black">Sân</b></label>
                                            <div class="col-sm-9">
                                                <div class="form-group label-floating is-empty">
                                                    <div class="col-lg-5 col-md-6 col-sm-3">
                                                        <select name="pitch_id" class="selectpicker"
                                                            data-style="btn btn-primary btn-round">
                                                            <option value="{{ $pitch->id }}">
                                                                {{ $pitch->pitch_name }}
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-sm-3 label-on-left"><b style="color: black">Ngày </b><b
                                                    style="color: red">*</b></label>
                                            <div class="col-sm-9">
                                                <div class="col-lg-5 col-md-6 col-sm-3">
                                                    <div class="form-group label-floating is-empty">
                                                        <input type="date" name="day" value="{{ $search }}"
                                                            min="{{ $today }}" readonly
                                                            class="form-control datepicke">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-sm-3 label-on-left"><b style="color: black">Khung
                                                    giờ</b></label>
                                            <div class="col-sm-9">
                                                <div class="form-group label-floating is-empty">
                                                    <div class="col-md-8">
                                                        <select name="time_id" class="selectpicker"
                                                            data-style="btn btn-primary btn-round">
                                                            @if ($empty_time_id->count() == 0)
                                                                <option value="0">Sân đã kín lịch</option>
                                                            @else
                                                                @foreach ($empty_time_id as $empty)
                                                                    <option value="{{ $empty->id }}">
                                                                        {{ $empty->time_start . '-' . $empty->time_end }}
                                                                    </option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-sm-3 label-on-left"><b style="color: black">Giá sân/1h
                                                    (VNĐ)</b></label>
                                            <div class="col-sm-9">
                                                <div class="form-group label-floating is-empty">
                                                    <input type="text" name="price" value="{{ $pitch->price }}"
                                                        style="font-size:20px; color:red" class="form-control datepicker"
                                                        readonly required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-sm-3 label-on-left"><b style="color: black">Số tiền đặt(VNĐ)
                                                </b> <b style="color: red">*</b>
                                                <p>( Bạn có thể trả trước 1 phần hoặc trả hết số tiền )</p>
                                            </label>
                                            <div class="col-sm-9">
                                                <div class="form-group label-floating is-empty">
                                                    <input type="text" name="deposit" placeholder="Nhập số tiền đặt cọc..."
                                                        class="form-control datepicker" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">

                                            </div>
                                            <div class="col-md-6">
                                                <a href="/pitch">
                                                    <button type="button" class="btn btn-secondary">Hủy</button>
                                                </a>
                                                <button class="btn btn-primary">Đặt</button>
                                            </div>
                                            <div class="col-md-2">

                                            </div>
                                        </div>
                                    </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
