@extends('layout.customer')

@section('content')
    <div class="section">
        <div class="container">
            {{-- success --}}
            @if ($message = Session::get('success'))
                <div class="section cd-section section-notifications" id="notifications">
                    <div class="alert alert-success">
                        <div>
                            <div class="alert-icon">
                                <i class="material-icons">check</i>
                            </div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="material-icons">clear</i></span>
                            </button>
                            <h3>{{ $message }}</h3>
                        </div>
                    </div>
                </div>
            @endif
            {{-- error --}}
            @if ($message = Session::get('errors'))
                <div class="section cd-section section-notifications" id="notifications">
                    <div class="alert alert-danger">
                        <div>
                            <div class="alert-icon">
                                <i class="material-icons">clear</i>
                            </div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="material-icons">clear</i></span>
                            </button>
                            <h3>{{ $message }}</h3>
                        </div>
                    </div>
                </div>
            @endif
            <h2 class="section-title">Các khu vực</h2>
            <div class="row">
                @foreach ($listArea as $area)
                    <div class="col-sm-4">
                        <div class="card card-product card-plain">
                            <div class="card-image">
                                <a href="/pitch_page/{{ $area->id }}">
                                    <img src="{{ asset('images/' . $area->image_path) }}"
                                        style="width: 360px; height:360px">
                                </a>
                            </div>
                            <div class="card-content">
                                <h4 class="card-title">
                                    <a href="/pitch_page/{{ $area->id }}">{{ $area->area_name }}</a>
                                </h4>
                                <p class="card-description">
                                    {{ $area->area_address }}
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- section -->
    <div class="section section-blog">
        <div class="container">
            <h2 class="section-title">Bài Viết</h2>
            <div class="row">
                @foreach ($listBlog as $blog)
                    <div class="col-sm-4">
                        <div class="card card-background"
                            style="background-image: url({{ asset('images/' . $blog->image_path) }}); width: 360px; height:360px">
                            <div class="card-content">
                                <h6 class="category text-info">{{ $blog->date_submitted }}</h6>
                                <a href="/blog_detail/{{ $blog->id }}">
                                    <h3 class="card-title">{{ $blog->title }}</h3>
                                </a>
                                <p class="card-description"
                                    style="overflow: hidden;text-overflow: ellipsis;-webkit-line-clamp: 5; -webkit-box-orient: vertical; display: -webkit-box;">
                                    {{ $blog->content }}
                                </p>
                                <a href="/blog_detail/{{ $blog->id }}" class="btn btn-white btn-round">
                                    <i class="material-icons">subject</i> Đọc thêm
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- section -->
    <div class="page-header header-filter clear-filter header-small"
        style="background-image: url('{{ asset('images/1639195617.jpg') }}');" style="width:100%; heigth:500px">
        {{-- <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="text-center">
                        <h3 class="title"></h3>
                        <p class="description">

                        </p>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
@endsection
