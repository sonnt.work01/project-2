@extends('layout.about_us')

@section('content')
    <div class="profile-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-xs-offset-3">
                    <div class="profile">
                        <div class="avatar">
                            @foreach ($introduce as $data)
                                <img src="{{ asset('images/' . $data->avatar) }}"
                                    style="width:150px; height:150px; border-radius:50%">
                            @endforeach
                        </div>
                        <div class="name">
                            @foreach ($introduce as $data)
                                <h3 class="title">Công ty {{ $data->company }}</h3>
                            @endforeach
                            <h6>Company</h6>
                            <a href="#pablo" class="btn btn-just-icon btn-simple btn-dribbble"><i
                                    class="fa fa-dribbble"></i></a>
                            <a href="#pablo" class="btn btn-just-icon btn-simple btn-twitter"><i
                                    class="fa fa-twitter"></i></a>
                            <a href="#pablo" class="btn btn-just-icon btn-simple btn-pinterest"><i
                                    class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="description text-center">
                @foreach ($introduce as $data)
                    <p style="color: black; font-size: 20px;">
                        {{ $data->content }}
                    </p>
                @endforeach
            </div>
        </div>
    </div>
@endsection
