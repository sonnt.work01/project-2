@extends('layout.customer')

@section('content')
<div class="container">
    @if ($message = Session::get('success'))
    <div class="section cd-section section-notifications" id="notifications">
        <div class="alert alert-success">
            <div>
                <div class="alert-icon">
                    <i class="material-icons">check</i>
                </div>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="material-icons">clear</i></span>
                </button>
                <h3>{{ $message }}</h3>
            </div>
        </div>
    </div>
    @endif
    <h1>Sửa thông tin cá nhân</h1>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>           
                <th>Họ Tên</th>
                <th>Email</th>
                <th>Ngày sinh</th>
                <th>Giới tính</th>
                <th>Số điện thoại</th>
                <th>Hành động</th>
            </tr>
            <tr>
                <td>{{ $customer->name }}</td>
                <td width="25%">{{ $customer->email }}</td>
                <td>{{ $customer->date_birth }}</td>
                <td>{{ $customer->GenderName }}</td>
                <td>{{ $customer->phone }}</td>
                <td><a href="{{route('profile_customer.edit',$customer->id)}}" class="btn btn-success">Sửa</a></td>
            </tr>
        </table>
    </div>
</div>
@endsection