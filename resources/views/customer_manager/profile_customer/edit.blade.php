@extends('layout.customer')

@section('content')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="section cd-section section-notifications" id="notifications">
                <div class="alert alert-success">
                    <div>
                        <div class="alert-icon">
                            <i class="material-icons">check</i>
                        </div>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="material-icons">clear</i></span>
                        </button>
                        <h3>{{ $message }}</h3>
                    </div>
                </div>
            </div>
        @endif
        {{-- error --}}
        @if ($message = Session::get('error'))
            <div class="section cd-section section-notifications" id="notifications">
                <div class="alert alert-danger">
                    <div>
                        <div class="alert-icon">
                            <i class="material-icons">clear</i>
                        </div>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="material-icons">clear</i></span>
                        </button>
                        <h3>{{ $message }}</h3>
                    </div>
                </div>
            </div>
        @endif
        <a href="{{ URL::previous() }}" class="btn btn-primary">Quay lại</a>
        <h1>Sửa thông tin cá nhân</h1>
        {{-- validate --}}
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li><span style="font-size: 19px">{{ $error }}</span></li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{ route('profile_customer.update', $customer->id) }}" method="post">
            @method('PUT')
            @csrf
            <table class="table table-striped">
                <tr>
                    <th>
                        Họ Tên
                    </th>
                    <td><input type="text" name="name" value="{{ $customer->name }}" class="form-control" required></td>
                </tr>
                <tr>
                    <th>
                        Email
                    </th>
                    <td><input type="text" name="email" value="{{ $customer->email }}" class="form-control" readonly
                            required></td>
                </tr>
                <tr>
                    <th>
                        Ngày sinh
                    </th>
                    <td><input type="date" name="date_birth" value="{{ $customer->date_birth }}" class="form-control"
                            required></td>
                </tr>
                <tr>
                    <th>
                        Giới tính
                    </th>
                    <td>
                        <input type="radio" name="gender" value="1" id="1" @if ($customer->gender == 1) checked @endif><label for="1"
                            style="color: black"><b>Nam</b></label>
                        <input type="radio" name="gender" value="0" id="0" @if ($customer->gender == 0) checked @endif><label for="0"
                            style="color: black"><b>Nữ</b></label>
                    </td>
                </tr>
                <tr>
                    <th>
                        Số điện thoại
                    </th>
                    <td><input type="text" name="phone" value="{{ $customer->phone }}" class="form-control" required>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button class="btn btn-info">Cập nhật</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
@endsection
