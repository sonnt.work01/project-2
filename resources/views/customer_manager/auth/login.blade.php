<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="{{ asset('assets') }}/img/football-icon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>BKSport Đăng nhập</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- CSS Files -->
    <link href="{{ asset('assets') }}/css/bootstrap.min.css" rel="stylesheet" />
    <link href="{{ asset('assets') }}/css/material-kit.css?v=1.2.1" rel="stylesheet" />
</head>

<body class="login-page">
    <nav class="navbar navbar-primary navbar-transparent navbar-absolute">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Bách Khoa Sport</a>
            </div>
        </div>
    </nav>

    <div class="page-header header-filter"
        style="background-image: url('{{ asset('assets') }}/img/login.jpg'); background-size: cover; background-position: top center;">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                    <div class="card card-signup">
                        <form class="form" method="post" action="{{ route('login-process') }}">
                            @csrf
                            <div class="header header-primary text-center">
                                <h4 class="card-title">Đăng nhập</h4>
                            </div>
                            <div class="card-content">
                                {{-- validate --}}
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li><span style="font-size: 17px">{{ $error }}</span></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                @if (Session::exists('success'))
                                    <div class="alert alert-success" align="center">
                                        <span style="font-size: 14px">{{ Session::get('success') }}</span>
                                    </div>
                                @endif
                                @if (Session::exists('error'))
                                    <div class="alert alert-danger" align="center">
                                        <span style="font-size: 14px">{{ Session::get('error') }}</span>
                                    </div>
                                @endif
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">email</i>
                                    </span>
                                    <input type="text" name="email" class="form-control" placeholder="Email...">
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">lock_outline</i>
                                    </span>

                                    <input type="password" name="password" placeholder="Password..."
                                        class="form-control" id="passIn" />
                                    {{-- <input type="checkbox" onclick="show_hide_pass()"> --}}
                                    <span class="material-icons" onclick="show_hide_pass()" style="cursor: pointer">
                                        visibility
                                    </span>
                                </div>
                                <a style="color: rgb(0, 14, 204);" href="/forget_password.html">Bạn quên mật khẩu?</a>
                            </div>
                            <div class="footer text-center">
                                <button class="btn btn-primary btn-simple btn-wd btn-lg">Đăng nhập</button>
                            </div>
                        </form>
                        <div class="footer text-center">
                            <p>Bạn chưa có tài khoản?<a style="color: blue" href="/signup">Đăng ký</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
<script type="text/javascript">
    function show_hide_pass(params) {
        var x = document.getElementById("passIn");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
</script>
<!--   Core JS Files   -->
<script src="{{ asset('assets') }}/js/jquery.min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{ asset('assets') }}/js/material.min.js"></script>

<!--    Plugin for Date Time Picker and Full Calendar Plugin   -->
<script src="{{ asset('assets') }}/js/moment.min.js"></script>

<!--	Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/   -->
<script src="{{ asset('assets') }}/js/nouislider.min.js" type="text/javascript"></script>

<!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker   -->
<script src="{{ asset('assets') }}/js/bootstrap-datetimepicker.js" type="text/javascript"></script>

<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select   -->
<script src="{{ asset('assets') }}/js/bootstrap-selectpicker.js" type="text/javascript"></script>

<!--	Plugin for Tags, full documentation here: http://xoxco.com/projects/code/tagsinput/   -->
<script src="{{ asset('assets') }}/js/bootstrap-tagsinput.js"></script>

<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput   -->
<script src="{{ asset('assets') }}/js/jasny-bootstrap.min.js"></script>

<!--    Plugin For Google Maps   -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

<!--    Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc    -->
<script src="{{ asset('assets') }}/js/material-kit.js?v=1.2.1" type="text/javascript"></script>

</html>
