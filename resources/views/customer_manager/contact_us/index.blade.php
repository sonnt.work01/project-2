@extends('layout.about_us')

@section('content')
    <div class="profile-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-xs-offset-3">
                    <div class="profile">
                        <div class="avatar">
                            @foreach ($introduce as $data)
                                <img src="{{ asset('images/' . $data->avatar) }}"
                                    style="width:150px; height:150px; border-radius:50%">
                            @endforeach
                        </div>
                        <div class="name">
                            @foreach ($introduce as $data)
                                <h3 class="title">Công ty {{ $data->company }}</h3>
                            @endforeach
                            <h6>Company</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach ($contact as $data)
                    <div class="col-sm-6">
                        <div>
                            <h2 style="color: red"><b>Khu vực {{ $data->area_name }}</b></h2>
                        </div>
                        <div>
                            <b>Địa chỉ:</b> {{ $data->area_address }}
                        </div>
                        <div>
                            <b>Hotline:</b> <b style="color: red">{{ $data->phone }}</b>
                        </div>
                        <hr>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
