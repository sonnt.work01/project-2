@extends('layout.blog')

@section('content')
    <div class="container">
        <div class="section section-text">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    @if ($message = Session::get('error'))
                        <div class="section cd-section section-notifications" id="notifications">
                            <div class="alert alert-danger">
                                <div>
                                    <div class="alert-icon">
                                        <i class="material-icons">check</i>
                                    </div>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
                                    </button>
                                    <h3>{{ $message }}</h3>
                                </div>
                            </div>
                        </div>
                    @endif
                    <article>
                        <h3 class="title">{{ $blog->title }}</h3>
                        <p>{{ $blog->content }}</p>
                    </article>

                    {{-- </div>

            <div class="section col-md-10 col-md-offset-1">
                <div class="col-md-4">
                    <img class="img-rounded img-responsive img-raised" alt="Raised Image" src="{{ asset('assets') }}/img/examples/blog4.jpg">
                </div>
                <div class="col-md-4">
                    <img class="img-rounded img-responsive img-raised" alt="Raised Image" src="{{ asset('assets') }}/img/examples/blog3.jpg">
                </div>
                <div class="col-md-4">
                    <img class="img-rounded img-responsive img-raised" alt="Raised Image" src="{{ asset('assets') }}/img/examples/blog1.jpg">
                </div> --}}
                </div>
            </div>
        </div>

        <div class="section section-blog-info">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="blog-tags">
                                Tags:
                                <span class="label label-primary">Vietnamese football</span>
                                <span class="label label-primary">Cong Phuong</span>
                                <span class="label label-primary">World Football</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <a href="#pablo" class="btn btn-google btn-round pull-right">
                                <i class="fa fa-google"></i> 232
                            </a>
                            <a href="#pablo" class="btn btn-twitter btn-round pull-right">
                                <i class="fa fa-twitter"></i> 910
                            </a>
                            <a href="#pablo" class="btn btn-facebook btn-round pull-right">
                                <i class="fa fa-facebook-square"></i> 872
                            </a>
                        </div>
                    </div>
                    <hr />
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="title text-center">Bài viết khác</h2>
                    <br />
                    <div class="row">
                        @foreach ($listBlog as $blog)
                            <div class="col-md-4">
                                <div class="card card-blog">
                                    <div class="card-image">
                                        <a href="/blog_detail/{{ $blog->id }}">
                                            <img class="img img-raised" src="{{ asset('images/' . $blog->image_path) }}"
                                                style="width:330px; height:330px" />
                                        </a>
                                    </div>
                                    <div class="card-content">
                                        <h6 class="category text-info">{{ $blog->date_submitted }}</h6>
                                        <h4 class="card-title">
                                            <a href="/blog_detail/{{ $blog->id }}">{{ $blog->title }}</a>
                                        </h4>
                                        <p class="card-description"
                                            style="overflow: hidden;text-overflow: ellipsis;-webkit-line-clamp: 5; -webkit-box-orient: vertical; display: -webkit-box;">
                                            {{ $blog->content }}
                                        </p>
                                        <a href="/blog_detail/{{ $blog->id }}">
                                            <h4>Xem thêm...</h4>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
