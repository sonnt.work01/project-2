@extends('layout.customer')

@section('content')
    <div class="container">
        <div class="material-datatables">
            <div>
                <h1 style="text-align:center">Danh sách hóa đơn</h1>
            </div>
            @if ($message = Session::get('success'))
                <div class="section cd-section section-notifications" id="notifications">
                    <div class="alert alert-success">
                        <div>
                            <div class="alert-icon">
                                <i class="material-icons">check</i>
                            </div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="material-icons">clear</i></span>
                            </button>
                            <h3>{{ $message }}</h3>
                        </div>
                    </div>
                </div>
            @endif
            @if ($message = Session::get('error'))
                <div class="section cd-section section-notifications" id="notifications">
                    <div class="alert alert-danger">
                        <div>
                            <div class="alert-icon">
                                <i class="material-icons">check</i>
                            </div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="material-icons">clear</i></span>
                            </button>
                            <h3>{{ $message }}</h3>
                        </div>
                    </div>
                </div>
            @endif
            <form action="" method="get">
                @csrf
                <div class="col-md-3 col-sm-2">
                    <select name="bill_active" class="selectpicker" data-style="btn btn-primary btn-round">
                        <option value="1" @if ($bill_active == 1) ? selected @endif>chưa xử lý</option>
                        <option value="2" @if ($bill_active == 2) ? selected @endif>đã đặt</option>
                        <option value="3" @if ($bill_active == 3) ? selected @endif>đã huỷ</option>
                        <option value="4" @if ($bill_active == 4) ? selected @endif>đã thanh toán</option>
                    </select>
                </div>
                <button class="btn btn-info">Tìm kiếm</button>
            </form>
            @if ($listBill->count() == 0)
                {{ 'Không có bản ghi' }}
            @else
                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0"
                    width="100%" style="width:100%">
                    <thead>
                        <tr>
                            <th>
                                Tên khách hàng
                            </th>
                            <th>
                                Tên khu vực
                            </th>
                            <th>
                                Tên sân
                            </th>
                            <th>
                                Ngày
                            </th>
                            <th>
                                Thời gian
                            </th>
                            <th>
                                Giá sân
                            </th>
                            <th>
                                Đặt cọc
                            </th>
                            <th>
                                Tiền còn lại
                            </th>
                            <th>
                                Trạng thái
                            </th>
                            @if ($bill_active == 1)
                                <th>
                                    Hành động
                                </th>
                            @endif
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>
                                Tên khách hàng
                            </th>
                            <th>
                                Tên khu vực
                            </th>
                            <th>
                                Tên sân
                            </th>
                            <th>
                                Ngày
                            </th>
                            <th>
                                Thời gian
                            </th>
                            <th>
                                Giá sân
                            </th>
                            <th>
                                Đặt cọc
                            </th>
                            <th>
                                Tiền còn lại
                            </th>
                            <th>
                                Trạng thái
                            </th>
                            @if ($bill_active == 1)
                                <th>
                                    Hành động
                                </th>
                            @endif
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($listBill as $listBill)
                            <tr>
                                <td>
                                    {{ $listBill->name }}
                                </td>
                                <td>
                                    {{ $listBill->area_name }}
                                </td>
                                <td>
                                    {{ $listBill->pitch_name }}
                                </td>
                                <td>
                                    {!! date('d-m-Y', strtotime($listBill->day)) !!}
                                </td>
                                <td>
                                    {{ $listBill->time_start . '-' . $listBill->time_end }}
                                </td>
                                <td>
                                    {{ $listBill->price }}
                                </td>
                                <td>
                                    {{ $listBill->deposit }}
                                </td>
                                <td>
                                    {{ $listBill->missing }}
                                </td>
                                <td>
                                    @if ($listBill->active == 1)
                                        <b style="color: blue">{{ $listBill->ActiveName }}</b>
                                    @elseif($listBill->active == 2)
                                        <b style="color: rgb(42, 202, 2)">{{ $listBill->ActiveName }}</b>
                                    @elseif($listBill->active == 3)
                                        <b style="color: rgb(255, 0, 0)">{{ $listBill->ActiveName }}</b>
                                    @elseif($listBill->active == 4)
                                        <b style="color: rgb(255, 103, 2)">{{ $listBill->ActiveName }}</b>
                                    @endif
                                </td>
                                @if ($listBill->active == 1)
                                    <td>
                                        <form action="/destroy_process/{{ $listBill->id }}" method="post">
                                            @csrf
                                            @method('POST')
                                            <button onclick="return confirm('Bạn có chắc chắn hủy đơn này?')"
                                                class="btn btn-danger">Huỷ đơn</button>
                                        </form>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
